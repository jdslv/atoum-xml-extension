
# jdslv/atoum-xml-extension

> The atoum XML extension allows you to make assertions on XML files

[![GitLab](https://img.shields.io/static/v1?message=GitLab&logo=gitlab&color=grey&label=)](https://gitlab.com/jdslv/atoum-xml-extension)
[![Latest stable version](https://img.shields.io/packagist/v/jdslv/atoum-xml-extension)](https://packagist.org/packages/jdslv/atoum-xml-extension)
[![Build status](https://gitlab.com/jdslv/atoum-xml-extension/badges/main/pipeline.svg)](https://gitlab.com/jdslv/atoum-xml-extension/-/pipelines)
[![Coverage status](https://img.shields.io/codecov/c/gitlab/jdslv/atoum-xml-extension)](https://codecov.io/gl/jdslv/atoum-xml-extension/)
[![Minimal PHP version](https://img.shields.io/packagist/php-v/jdslv/atoum-xml-extension)](https://gitlab.com/jdslv/atoum-xml-extension)
[![License](https://img.shields.io/packagist/l/jdslv/atoum-xml-extension)](https://gitlab.com/jdslv/atoum-xml-extension/-/blob/main/LICENSE)


This atoum extension allows you to test XML document using [atoum](https://github.com/atoum/atoum).

This repository is based on [shulard/atoum-xml-extension](https://github.com/shulard/atoum-xml-extension).


## Install it

Install extension using [composer](https://getcomposer.org):

```
composer require --dev jdslv/atoum-xml-extension
```

The extension is automatically added to atoum configuration.


## Use it

Add the following code to your configuration file:

```php
<?php

// .atoum.php

$runner->addExtension(new atoum\atoum\xml\extension($script));
```

```php
<?php

// tests example

namespace my\project\tests\unit;

use atoum;

class MyClass extends atoum\atoum\test
{
    public function testXML()
    {
        $this
            ->if($xml = <<<XML
<?xml version="1.0" ?>
<root>
    <node attribute="value" />
    <node m:attribute="namespaced value" />
</root>
XML)
            ->then
                ->xml($xml)
                    ->attributes
                        ->areEmpty
                    ->namespaces
                        ->areEmpty

                    ->nodes
                        ->hasSize(2)
                        ->first
                            ->isTag('node')
                            ->attributes
                                ->hasSize(1)
                                ->hasKey('attribute')
                                ->string['attribute']
                                    ->isIdenticalTo('value')
                                    ->hasNotNamespace
                            ->content
                                ->isEmpty

                        ->next
                            ->isTag('node')
                            ->attributes
                                ->hasSize(1)
                                ->hasKey('m:attribute')
                                ->string['attribute']
                                    ->isIdenticalTo('namespaced value')
                                    ->hasNamespace('m')
                                ->string['m:attribute']
                                    ->isIdenticalTo('namespaced value')
                            ->content
                                ->isEmpty
        ;
    }
}
```


## Asserters

Every asserters allow a final `string` argument to personalize error message.

They are all fluent, you can chain assertions, we automatically find the better context for your asserters.

You should also know that all assertions without parameter can be written with or without parenthesis.
So `$this->integer(0)->isZero()` is the same as `$this->integer(0)->isZero`.
