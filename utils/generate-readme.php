<?php

/*
Really dirty script to generate the "asserters" part of the README.

Dont judge me, I was tired :)
*/

require __DIR__ . '/../vendor/autoload.php';

define('VERBOSE', false);

$ns = 'atoum\atoum\xml\asserters\\';

$asserters = [];
$data = json_decode(file_get_contents(__DIR__ . '/data.json'), true);

if ($handle = opendir(__DIR__ . '/../src/asserters')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry !== '.' && $entry !== '..') {
            $classname = str_replace('.php', '', $entry);
            $cls = $ns . $classname;
            $reflection = new ReflectionClass($cls);
            $reflectionName = $reflection->getName();

            if ($classname !== 'xml' && strpos($classname, 'xml') === 0) {
                $classname = lcfirst(substr($classname, 3));
            }

            if (!array_key_exists($classname, $data)) {
                continue;
            }

            $asserters[$classname] = [
                'fullname' => $reflection->getName(),
                'methods' => [],
                'name' => $classname,
            ];

            $doc = $reflection->getDocComment();

            if ($doc) {
                $clean = function ($v) {
                    return ltrim($v, ' *');
                };
                $tmp = trim(implode("\n", array_map($clean, explode("\n", substr($doc, 3, -2)))));

                $asserters[$classname]['desc'] = 'It\'s the ' . lcfirst($tmp);
            }

            foreach ($reflection->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                $name = $method->getName();
                $parentName = $method->getDeclaringClass()->getName();
                $methodData = [
                    'inherited' => false,
                    'link' => null,
                    'name' => $name,
                ];

                if ($parentName !== $reflectionName) {
                    $methodData['link'] = '';

                    if (strpos($parentName, $ns) === 0) {
                        $methodData['inherited'] = str_replace($ns, '', $parentName);
                    } elseif ($parentName === 'atoum\atoum\xml\asserter') {
                        $methodData['inherited'] = 'asserter';
                    } else {
                        $methodData['inherited'] = str_replace('atoum\atoum\asserters\\', '', $parentName);
                        $methodData['link'] = 'https://atoum.readthedocs.io/en/latest/asserters.html';

                        if (strpos($methodData['inherited'], 'php') === 0) {
                            $methodData['inherited'] = lcfirst(substr($methodData['inherited'], 3));
                        }
                    }

                    $tmp = preg_replace('/([a-z])([A-Z])/', '\1-\2', $methodData['inherited'] . '-' . $name);

                    $methodData['link'] .= '#' . strtolower($tmp);
                }

                $reference = $name;

                if (strpos($name, 'isNot') === 0) {
                    $reference = str_replace('isNot', 'is', $name);
                }

                if (strpos($name, 'not') === 0) {
                    $reference = lcfirst(str_replace('not', '', $name));
                }

                if (strpos($name, 'hasNot') === 0) {
                    $reference = str_replace('hasNot', 'has', $name);
                }

                if (!array_key_exists($reference, $data[$classname])) {
                    if (VERBOSE) {
                        echo 'Skip ', $classname, '::', $name, "\n";
                    }

                    continue;
                }

                if (VERBOSE) {
                    echo 'Process ', $classname, '::', $name, "\n";
                }

                $methodData['data'] = $data[$classname][$reference] ?? [];

                $doc = $method->getDocComment();

                if ($doc) {
                    $clean = function ($v) {
                        $line = ltrim($v, ' *');

                        if (str_starts_with($line, '@')) {
                            return '';
                        }

                        return $line;
                    };
                    $tmp = trim(implode("\n", array_map($clean, explode("\n", substr($doc, 3, -2)))));

                    $methodData['desc'] = '`' . $name . '` ' . lcfirst($tmp);
                }

                $file = __DIR__ . '/xml/' . $classname . '/' . $reference . '.xml';

                if (is_file($file)) {
                    $methodData['file'] = $file;
                }

                $params = [];

                foreach ($method->getParameters() as $parameter) {
                    $type = '';
                    $default = '';

                    if ($parameter->isDefaultValueAvailable()) {
                        if ($parameter->getDefaultValue() === null) {
                            $default = '= null';
                        } else {
                            $default = sprintf('= \'%s\'', $parameter->getDefaultValue());
                        }
                    }

                    if ($parameter->getType()) {
                        $type = (string) $parameter->getType();
                    }

                    $paramName = '$' . $parameter->getName();

                    if ($paramName === '$failMessage' || $paramName === '$message') {
                        $type = '?string';
                        $paramName = '$message';
                        $default = '= null';
                    }

                    $params[] = trim($type . ' ' . $paramName . ' ' . $default);
                }

                $return = $method->getReturnType();
                $returnType = '';

                if ($return) {
                    $returnType = (string) $return;
                }

                if (strpos($returnType, 'xml') === 0) {
                    $returnType = lcfirst(substr($returnType, 3));
                }

                if ($returnType) {
                    $returnType = ': ' . $returnType;
                }

                $methodData['signature'] = 'public function ' . $name . '(' . implode(', ', $params) . ')' . $returnType;

                $asserters[$classname]['methods'][] = $methodData;

                if (in_array($name, ['isEmpty', 'isNotEmpty'], true) && str_ends_with($classname, 's')) {
                    $newName = str_replace('is', 'are', $name);
                    $methodData['name'] = $newName;

                    foreach (['desc', 'signature'] as $key) {
                        $methodData[$key] = str_replace($name, $newName, $methodData[$key]);
                    }

                    $asserters[$classname]['methods'][] = $methodData;
                }

                usort($asserters[$classname]['methods'], function ($a, $b) {
                    return strcmp($a['name'], $b['name']);
                });
            }
        }
    }

    closedir($handle);
}

usort($asserters, function ($a, $b) {
    return strcmp($a['name'], $b['name']);
});

$atoum = new atoum\atoum\xml\asserters\xml();
$atoum->getGenerator()->addNamespace($ns);

$file = fopen(__DIR__ . '/../README.md', 'w');

fwrite($file, file_get_contents(__DIR__ . '/head.md'));

foreach ($asserters as $asserter) {
    if (VERBOSE) {
        echo "\n", 'Writing ' . $asserter['name'], "\n";
    }

    fwrite($file, "\n### " . $asserter['name'] . "\n\n");

    if (array_key_exists('desc', $asserter)) {
        fwrite($file, $asserter['desc'] . "\n\n");
    }

    foreach ($asserter['methods'] as $method) {
        if (VERBOSE) {
            echo 'Writing ' . $asserter['name'] . '::' . $method['name'], "\n";
        }

        fwrite($file, '#### ' . $method['name'] . "\n\n");

        if (array_key_exists('desc', $method)) {
            fwrite($file, $method['desc'] . "\n\n");
        }

        fwrite($file, "```\n" . $method['signature'] . "\n```\n\n");

        if ($method['inherited'] ?? false) {
            fwrite($file, <<<MD
> `{$method['name']}` is a method inherited from the `{$method['inherited']}` asserter.
For more information, refer to the documentation of [{$method['inherited']}::{$method['name']}]({$method['link']}).



MD);
        }

        if (array_key_exists('file', $method)) {
            $content = trim(file_get_contents($method['file']));
            $atoum->setWith($content);

            fwrite($file, <<<CODE
```php
<?php

\$xml = '{$content}';

\$this
    ->xml(\$xml)

CODE);

            testIt($asserter['name'], $method['name'], clone $atoum, $method['data'], $file, 8);

            fwrite($file, ";\n```\n\n");
        }
    }
}

function testIt(
    string $asserter,
    string $assertion,
    atoum\atoum\asserter $atoum,
    array $data,
    $file,
    int $indent,
    array &$lines = [],
    bool $output = true,
) {
    $quote = function ($v) use (&$quote) {
        if (is_numeric($v)) {
            return $v;
        }

        if (is_array($v)) {
            return '[' . implode(', ', array_map($quote, $v)) . ']';
        }

        return '\'' . $v . '\'';
    };

    foreach ($data['calls'] as $call) {
        $name = $call['name'];
        $args = $call['args'] ?? [];

        if ($name === '<<asserter>>') {
            $name = $asserter;
        }

        if ($name === '<<assertion>>') {
            $name = $assertion;
        }

        $line = str_pad('', $indent) . '->' . $name . '(' . implode(', ', array_map($quote, $args)) . ')';
        $success = true;

        try {
            $atoum = $atoum->{$name}(...$args);
        } catch (atoum\atoum\asserter\exception $exc) {
            $success = false;
        }

        if (array_key_exists('calls', $call)) {
            $lines[] = [
                'text' => $line,
            ];

            testIt($asserter, $assertion, $atoum, $call, $file, $indent + 4, $lines, false);
        } else {
            $lines[] = [
                'comment' => $success ? 'succeed' : 'failed',
                'text' => $line,
            ];
        }
    }

    if ($output) {
        $max = array_reduce($lines, function ($prev, $curr) {
            return max($prev, strlen($curr['text']) + 2);
        }, 0);

        foreach ($lines as $line) {
            $comment = '';

            if (array_key_exists('comment', $line)) {
                $comment = str_pad('', $max - strlen($line['text'])) . '// ' . $line['comment'];
            }

            fwrite($file, $line['text'] . $comment . "\n");
        }
    }
}

fwrite($file, <<<'LICENSE'

## License

`jdslv/atoum-xml-extension` is released under the Apache 2.0 License.
See the bundled [LICENSE](LICENSE) file for details.

![atoum](http://atoum.org/images/logo/atoum.png)

LICENSE);
