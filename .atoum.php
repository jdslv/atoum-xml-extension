<?php

$runner
    ->addTestsFromDirectory(__DIR__ . '/tests/units')
    ->addTestsFromDirectory(__DIR__ . '/tests/functionals')
;

// Show default report
$script->addDefaultReport();

// reports
if (extension_loaded('xdebug') === true) {
    $script->noCodeCoverageInDirectories(__DIR__ . '/vendor');

    $coverage = new atoum\atoum\reports\coverage\html();
    $coverage
        ->addWriter(new atoum\atoum\writers\std\out())
        ->setOutPutDirectory('./reports')
    ;
    $runner->addReport($coverage);

    // coverage report
    $cobertura = new atoum\atoum\reports\cobertura();
    $cobertura->addWriter(new atoum\atoum\writers\file('reports/cobertura.xml'));
    $runner->addReport($cobertura);

    // xunit report
    $xunit = new atoum\atoum\reports\sonar\xunit();
    $xunit->addWriter(new atoum\atoum\writers\file('reports/junit.xml'));
    $runner->addReport($xunit);
}
