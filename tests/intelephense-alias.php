<?php

namespace mock;

class stdClass extends \stdClass {}

namespace mock\atoum\atoum;

class adapter extends \atoum\atoum\adapter {}
class configurator extends \atoum\atoum\configurator {}
class observable extends \atoum\atoum\observable {}
class runner extends \atoum\atoum\runner {}
class test extends \atoum\atoum\test {}

namespace mock\atoum\atoum\script\arguments;

class parser extends \atoum\atoum\script\arguments\parser {}

namespace mock\atoum\atoum\test\assertion;

class manager extends \atoum\atoum\test\assertion\manager {}

namespace mock\atoum\atoum\xml\asserters;

class node extends \atoum\atoum\xml\asserters\node {}
