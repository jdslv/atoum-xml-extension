<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;
use DomImplementation;
use SimpleXmlElement;

class xml extends atoum\atoum\test
{
    public function test__get()
    {
        $this
            ->if($asserter = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$asserter})
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\xml\asserters\node::class)
        ;
    }

    public function testNamespace()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->namespace(uniqid(), uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With an unknown namespace')
                ->given($prefix = uniqid())
                ->and($uri = uniqid())
                ->and($xml = '<?xml version="1.0"?><root/>')

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($ns = $this->testedInstance->namespace($prefix, $uri))
                        ->isInstanceOf(atoum\atoum\xml\asserters\xmlNamespace::class)

                    ->object($ns->parent)
                        ->isTestedInstance

                    ->array($ns->getValue())
                        ->isEmpty

            ->assert('With a known namespace')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s"><item %1$s:href="%3$s" /></root>', [
                    $prefix,
                    $uri,
                    '#link',
                ]))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($ns = $this->testedInstance->namespace($prefix, $uri))
                        ->isInstanceOf(atoum\atoum\xml\asserters\xmlNamespace::class)

                    ->object($ns->parent)
                        ->isTestedInstance

                    ->array($ns->getValue())
                        ->hasKeys(['isUsed', 'prefix', 'uri'])
                        ->isEqualTo(['isUsed' => true, 'prefix' => $prefix, 'uri' => $uri])

            ->assert('With bad URI')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s"><item %1$s:href="%3$s" /></root>', [
                    $prefix,
                    uniqid(),
                    '#link',
                ]))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($ns = $this->testedInstance->namespace($prefix, $uri))
                        ->isInstanceOf(atoum\atoum\xml\asserters\xmlNamespace::class)

                    ->object($ns->parent)
                        ->isTestedInstance

                    ->array($ns->getValue())
                        ->isEmpty
        ;
    }

    public function testNamespaces()
    {
        $this
            ->assert('Without value, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->namespaces())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->namespaces)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With value, called as method')
                ->if($this->newTestedInstance->setWith('<?xml version="1.0"?><root/>'))
                ->then
                    ->object($ns = $this->testedInstance->namespaces())
                        ->isInstanceOf(atoum\atoum\xml\asserters\namespaces::class)

                    ->object($ns->parent)
                        ->isTestedInstance

            ->assert('With value, called as property')
                ->if($this->newTestedInstance->setWith('<?xml version="1.0"?><root/>'))
                ->then
                    ->object($ns = $this->testedInstance->namespaces)
                        ->isInstanceOf(atoum\atoum\xml\asserters\namespaces::class)

                    ->object($ns->parent)
                        ->isTestedInstance
        ;
    }

    public function testRoot()
    {
        $this
            ->assert('Without value, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->root())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->root)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With value, called as method')
                ->given($xml = '<?xml version="1.0"?><root/>')

                ->if($this->testedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->root())
                        ->isTestedInstance

            ->assert('With value, called as property')
                ->given($xml = '<?xml version="1.0"?><root/>')

                ->if($this->testedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->root)
                        ->isTestedInstance
        ;
    }

    public function testSetWith()
    {
        $this
            ->assert('Null value')
                ->exception(fn () => $this->newTestedInstance->setWith(null))
                    ->isInstanceOf(atoum\atoum\asserter\exception::class)
                    ->hasMessage('XML is undefined')

            ->assert('With not a string')
                ->if($value = random_int(0, PHP_INT_MAX))
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid XML', $value))

            ->assert('With not a XML')
                ->if($value = uniqid())
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid XML', $value))

            ->assert('With invalid XML')
                ->if($value = '<?xml version="1.0"?><root')
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid XML', $value))

            ->assert('With XML string')
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($xml = '<?xml version="1.0"?><root/>')
                ->then
                    ->object($this->testedInstance->setWith($xml))
                        ->isTestedInstance

                    ->integer($this->testedInstance->getTest()->getScore()->getAssertionNumber())
                        ->isEqualTo(1)

            ->assert('With DOM instance')
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($xml = (new DomImplementation())->createDocument('', 'root'))
                ->then
                    ->object($this->testedInstance->setWith($xml))
                        ->isTestedInstance

                    ->integer($this->testedInstance->getTest()->getScore()->getAssertionNumber())
                        ->isEqualTo(1)

            ->assert('With SimpleXML instance')
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->then
                    ->object($this->testedInstance->setWith($xml))
                        ->isTestedInstance

                    ->integer($this->testedInstance->getTest()->getScore()->getAssertionNumber())
                        ->isEqualTo(1)
        ;
    }
}
