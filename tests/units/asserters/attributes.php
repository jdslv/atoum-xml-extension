<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;

class attributes extends atoum\atoum\test
{
    public function test__call()
    {
        $this
            ->if($method = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$method}())
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))
        ;
    }

    public function test__get()
    {
        $this
            ->if($asserter = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$asserter})
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))
        ;
    }

    public function testAreEmpty_AreNotEmpty()
    {
        $this
            ->assert('Without value, areEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, areEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, areNotEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, areNotEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('With non empty element, areEmpty called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty element, areEmpty called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attributes are not empty, has size %d', 1))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty element, areNotEmpty called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->areNotEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With non empty element, areNotEmpty called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->areNotEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, areEmpty called as method')
                ->given($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->areEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, areEmpty called as property')
                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->areEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, areNotEmpty called as method')
                ->given($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With empty element, areNotEmpty called as property')
                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attributes are empty'))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\xml\asserters\variable::class)
        ;
    }

    public function testHasKey_NotHasKey()
    {
        $this
            ->assert('hasKey, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasKey(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('notHasKey, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKey(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('hasKey, With existing value')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasKey($name))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('notHasKey, With existing value')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKey($name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute key "%s" exists', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notHasKey, With existing value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKey($name, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasKey, With namespace')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasKey($name))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasKey($prefix . ':' . $name))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('notHasKey, With namespace')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKey($name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute key "%s" exists', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKey($prefix . ':' . $name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute key "%s" exists', $prefix . ':' . $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notHasKey, With namespace and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKey($name, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKey($prefix . ':' . $name, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasKey, With unknown attribute')
                ->given($name = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasKey($name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('No attribute key "%s" found', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasKey, With unknown attribute and custom message')
                ->given($name = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasKey($name, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notHasKey, With unknown attribute')
                ->given($name = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->notHasKey($name))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testHasKeys_NotHasKeys()
    {
        $this
            ->assert('hasKeys, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasKeys([uniqid()]))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('notHasKeys, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([uniqid()]))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('hasKeys, With existing value')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasKeys([$name]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('notHasKeys, With existing value')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([$name]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute key "%s" exists', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notHasKeys, With existing value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([$name], $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasKeys, With namespace')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasKeys([$name]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasKeys([$prefix . ':' . $name]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('notHasKeys, With namespace')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([$name]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute key "%s" exists', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([$prefix . ':' . $name]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute key "%s" exists', $prefix . ':' . $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notHasKeys, With namespace and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([$name], $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([$prefix . ':' . $name], $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasKeys, With unknown attribute')
                ->given($name = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasKeys([$name]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('No attribute key "%s" found', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasKeys, With unknown attribute and custom message')
                ->given($name = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasKeys([$name], $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notHasKeys, With unknown attribute')
                ->given($name = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->notHasKeys([$name]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('hasKeys, With multiple attributes')
                ->given($name1 = uniqid())
                ->and($value1 = uniqid())
                ->and($prefix1 = uniqid())
                ->and($url1 = uniqid())

                ->if($name2 = uniqid())
                ->and($value2 = uniqid())
                ->and($prefix2 = uniqid())
                ->and($url2 = uniqid())

                ->if($name3 = uniqid())
                ->and($message = uniqid())

                ->if($data = [
                    [
                        'name' => $name1,
                        'prefix' => $prefix1,
                        'url' => $url1,
                        'value' => $value1,
                    ],
                    [
                        'name' => $name2,
                        'prefix' => $prefix2,
                        'url' => $url2,
                        'value' => $value2,
                    ],
                ])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasKeys([$name3]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('No attribute key "%s" found', $name3))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasKeys([$name3], $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasKeys([$name1, $name2]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(2)

            ->assert('notHasKeys, With multiple attributes')
                ->given($name1 = uniqid())
                ->and($value1 = uniqid())
                ->and($prefix1 = uniqid())
                ->and($url1 = uniqid())

                ->if($name2 = uniqid())
                ->and($value2 = uniqid())
                ->and($prefix2 = uniqid())
                ->and($url2 = uniqid())

                ->if($name3 = uniqid())
                ->and($name4 = uniqid())
                ->and($message = uniqid())

                ->if($data = [
                    [
                        'name' => $name1,
                        'prefix' => $prefix1,
                        'url' => $url1,
                        'value' => $value1,
                    ],
                    [
                        'name' => $name2,
                        'prefix' => $prefix2,
                        'url' => $url2,
                        'value' => $value2,
                    ],
                ])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([$name1, $name2]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute key "%s" exists', $name1))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notHasKeys([$name1, $name2], $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->notHasKeys([$name3, $name4]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(2)
        ;
    }

    public function testHasSize()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX)))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('With negative value')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX) * -1))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value must be positive')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With negative value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX) * -1, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With bad size')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($size = random_int(2, PHP_INT_MAX))

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize($size))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attributes has size %d, expected size %d', 1, $size))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(2, PHP_INT_MAX), $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With correct size')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasSize(1))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testIsEmpty_IsNotEmpty()
    {
        $this
            ->assert('Without value, isEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, isEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, isNotEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, isNotEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('With non empty element, isEmpty called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty element, isEmpty called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attributes are not empty, has size %d', 1))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty element, isNotEmpty called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With non empty element, isNotEmpty called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, isEmpty called as method')
                ->given($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, isEmpty called as property')
                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, isNotEmpty called as method')
                ->given($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With empty element, isNotEmpty called as property')
                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attributes are empty'))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)
        ;
    }

    public function testOffsetExists()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->offsetExists(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('With existing value')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->then
                    ->boolean($this->testedInstance->offsetExists($name))
                        ->isTrue

            ->assert('With namespace')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->then
                    ->boolean($this->testedInstance->offsetExists($name))
                        ->isTrue

                    ->boolean($this->testedInstance->offsetExists($prefix . ':' . $name))
                        ->isTrue

            ->assert('With unknown attribute')
                ->given($name = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->then
                    ->boolean($this->testedInstance->offsetExists($name))
                        ->isFalse
        ;
    }

    public function testOffsetSet()
    {
        $this
            ->exception(fn () => $this->newTestedInstance->offsetSet(uniqid(), uniqid()))
                ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                ->hasMessage('Array is read only')
        ;
    }

    public function testOffsetUnset()
    {
        $this
            ->exception(fn () => $this->newTestedInstance->offsetUnset(uniqid()))
                ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                ->hasMessage('Array is read only')
        ;
    }

    public function testSetWith()
    {
        $this
            ->assert('Null value')
                ->exception(fn () => $this->newTestedInstance->setWith(null))
                    ->isInstanceOf(atoum\atoum\asserter\exception::class)
                    ->hasMessage('Value is undefined')

            ->assert('With invalid data')
                ->given($value = random_int(0, PHP_INT_MAX))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value must be an array')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With empty array')
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith([]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->array($this->testedInstance->getValue())
                        ->isEmpty

            ->assert('With missing name')
                ->given($value = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->setWith([['value' => $value]]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Name is missing in the attribute at position 0')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With missing value')
                ->given($name = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->setWith([['name' => $name]]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value is missing in the attribute at position 0')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With valid attribute data')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith([compact('name', 'value')]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With valid namespaced attribute data')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith([compact('name', 'value', 'prefix')]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('Null value in array')
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(function () {
                        $data = [
                            [
                                'name' => uniqid(),
                                'value' => uniqid(),
                            ],
                            null,
                        ];
                        $this->testedInstance->setWith($data);
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value is undefined at position 1')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With invalid data in array')
                ->given($value = random_int(0, PHP_INT_MAX))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(function () use ($value) {
                        $data = [
                            [
                                'name' => uniqid(),
                                'value' => uniqid(),
                            ],
                            $value,
                        ];
                        $this->testedInstance->setWith($data);
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value must be an array at position 1')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With missing name in array')
                ->given($value = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(function () use ($value) {
                        $data = [
                            [
                                'name' => uniqid(),
                                'value' => uniqid(),
                            ],
                            compact('value'),
                        ];
                        $this->testedInstance->setWith($data);
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Name is missing in the attribute at position 1')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With missing value in array')
                ->given($name = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(function () use ($name) {
                        $data = [
                            [
                                'name' => uniqid(),
                                'value' => uniqid(),
                            ],
                            compact('name'),
                        ];
                        $this->testedInstance->setWith($data);
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value is missing in the attribute at position 1')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)
        ;
    }

    public function testSize()
    {
        $this
            ->assert('Without value, method call')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->size())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, property call')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->size)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('With value, method call')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->then
                    ->object($this->testedInstance->size())
                        ->isInstanceOf(atoum\atoum\xml\asserters\integer::class)

                    ->integer($this->testedInstance->size()->getValue())
                        ->isEqualTo(1)

            ->assert('With value, property call')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->then
                    ->object($this->testedInstance->size)
                        ->isInstanceOf(atoum\atoum\xml\asserters\integer::class)

                    ->integer($this->testedInstance->size->getValue())
                        ->isEqualTo(1)
        ;
    }

    public function testString()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->string[uniqid()])
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('With value')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($asserter = $this->testedInstance->string[$name])
                        ->isInstanceOf(atoum\atoum\xml\asserters\attribute::class)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->array($asserter->getValue())
                        ->hasKeys(['name', 'value'])
                        ->notHasKeys(['prefix', 'url'])
                        ->string['name']->isEqualTo($name)
                        ->string['value']->isEqualTo($value)

            ->assert('With namespace')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($asserter = $this->testedInstance->string[$name])
                        ->isInstanceOf(atoum\atoum\xml\asserters\attribute::class)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->array($asserter->getValue())
                        ->hasKeys(['name', 'value', 'prefix', 'url'])
                        ->string['name']->isEqualTo($name)
                        ->string['prefix']->isEqualTo($prefix)
                        ->string['url']->isEqualTo($url)
                        ->string['value']->isEqualTo($value)

                ->if($this->newTestedInstance->setWith([compact('name', 'value', 'prefix', 'url')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                    ->object($asserter = $this->testedInstance->string[$prefix . ':' . $name])
                        ->isInstanceOf(atoum\atoum\xml\asserters\attribute::class)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->array($asserter->getValue())
                        ->hasKeys(['name', 'value', 'prefix', 'url'])
                        ->string['name']->isEqualTo($name)
                        ->string['prefix']->isEqualTo($prefix)
                        ->string['url']->isEqualTo($url)
                        ->string['value']->isEqualTo($value)

            ->assert('With multiple values')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith([
                    compact('name', 'value', 'prefix', 'url'),
                    compact('name', 'value'),
                ]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($asserter = $this->testedInstance->string[$name])
                        ->isInstanceOf(atoum\atoum\xml\asserters\attribute::class)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->array($asserter->getValue())
                        ->hasKeys(['name', 'value'])
                        ->notHasKeys(['prefix', 'url'])
                        ->string['name']->isEqualTo($name)
                        ->string['value']->isEqualTo($value)

                ->if($this->newTestedInstance->setWith([
                    compact('name', 'value', 'prefix', 'url'),
                    compact('name', 'value'),
                ]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($asserter = $this->testedInstance->string[$prefix . ':' . $name])
                        ->isInstanceOf(atoum\atoum\xml\asserters\attribute::class)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->array($asserter->getValue())
                        ->hasKeys(['name', 'value', 'prefix', 'url'])
                        ->string['name']->isEqualTo($name)
                        ->string['prefix']->isEqualTo($prefix)
                        ->string['url']->isEqualTo($url)
                        ->string['value']->isEqualTo($value)

            ->assert('With unknown attribute')
                ->given($name = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->string[$name])
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('No attribute "%s" found', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With duplicate attributes')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith([compact('name', 'value'), compact('name', 'value')]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->string[$name])
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Too much attributes "%s" found', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                ->if($this->newTestedInstance->setWith([
                    compact('name', 'value', 'prefix', 'url'),
                    compact('name', 'value', 'prefix', 'url'),
                ]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->string[$prefix . ':' . $name])
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Too much attributes "%s" found', $prefix . ':' . $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)
        ;
    }
}
