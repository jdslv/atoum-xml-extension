<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;

class xmlNamespace extends atoum\atoum\test
{
    public function test__get()
    {
        $this
            ->if($asserter = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$asserter})
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\xml\asserters\variable::class)
        ;
    }

    public function testExists_NotExists()
    {
        $this
            ->assert('Without value')
                ->exception(fn () => $this->newTestedInstance->exists())
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('XML is undefined')

                ->exception(fn () => $this->newTestedInstance->exists)
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('XML is undefined')

                ->exception(fn () => $this->newTestedInstance->notExists())
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('XML is undefined')

                ->exception(fn () => $this->newTestedInstance->notExists)
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('XML is undefined')

            ->assert('With an unknown namespace, default message')
                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->exists)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespace does not exist')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                    ->object($this->testedInstance->notExists)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With an unknown namespace, custom message')
                ->given($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->exists($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                    ->object($this->testedInstance->notExists($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With a known namespace, default message')
                ->given($prefix = uniqid())
                ->and($uri = uniqid())

                ->if($this->newTestedInstance->setWith(compact('prefix', 'uri')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->exists)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->exception(fn () => $this->testedInstance->notExists)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespace does exist')

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With a known namespace, custom message')
                ->given($prefix = uniqid())
                ->and($uri = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('prefix', 'uri')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->exists($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->exception(fn () => $this->testedInstance->notExists($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testIsUsed_IsNotUsed()
    {
        $this
            ->assert('Without value')
                ->exception(fn () => $this->newTestedInstance->isUsed())
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('XML is undefined')

                ->exception(fn () => $this->newTestedInstance->isUsed)
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('XML is undefined')

                ->exception(fn () => $this->newTestedInstance->isNotUsed())
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('XML is undefined')

                ->exception(fn () => $this->newTestedInstance->isNotUsed)
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('XML is undefined')

            ->assert('Without data, default message')
                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isUsed)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespace is not used')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                    ->object($this->testedInstance->isNotUsed)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('Without data, custom message')
                ->given($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isUsed($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                    ->object($this->testedInstance->isNotUsed($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With data set as true, default message')
                ->if($this->newTestedInstance->setWith(['isUsed' => true]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isUsed)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->exception(fn () => $this->testedInstance->isNotUsed)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespace is used')

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With data set as true, custom message')
                ->if($this->newTestedInstance->setWith(['isUsed' => true]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isUsed($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

                    ->exception(fn () => $this->testedInstance->isNotUsed($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With data set as false, default message')
                ->if($this->newTestedInstance->setWith(['isUsed' => false]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isUsed)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespace is not used')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                    ->object($this->testedInstance->isNotUsed)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With data set as false, custom message')
                ->if($this->newTestedInstance->setWith(['isUsed' => false]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isUsed($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

                    ->object($this->testedInstance->isNotUsed($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    /**
     * @dataProvider valueProvider
     */
    public function testSetWith(string $name, array $value)
    {
        $this
            ->assert($name)
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->boolean($this->testedInstance->wasSet())
                        ->isFalse

                    ->object($this->testedInstance->setWith($value))
                        ->isTestedInstance

                    ->boolean($this->testedInstance->wasSet())
                        ->isTrue

                    ->variable($this->testedInstance->getValue())
                        ->isIdenticalTo($value)

                    ->integer($score->getAssertionNumber())
                        ->isEqualTo(1)
        ;
    }

    public function testSetWith_invalid()
    {
        $this
            ->assert('Not an array')
                ->if($value = uniqid())
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" must be an array', $value))

            ->assert('Must have a prefix key')
                ->if($value = ['uri' => uniqid()])
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" must have `prefix` and `uri` keys', var_export($value, true)))

            ->assert('Must have a uri key')
                ->if($value = ['prefix' => uniqid()])
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" must have `prefix` and `uri` keys', var_export($value, true)))

            ->assert('isUsed key must be a boolean')
                ->if($value = ['isUsed' => uniqid()])
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('`isUsed` must be a boolean')
        ;
    }

    public function valueProvider()
    {
        yield ['Empty array, no namespace matched', []];

        yield ['Namespace exists', ['prefix' => uniqid(), 'uri' => uniqid()]];

        yield ['Namespace used', ['prefix' => uniqid(), 'uri' => uniqid(), 'isUsed' => true]];

        yield ['Namespace not used', ['prefix' => uniqid(), 'uri' => uniqid(), 'isUsed' => false]];

        yield ['No namespace / used', ['isUsed' => true]];

        yield ['No namespace / not used', ['isUsed' => false]];
    }
}
