<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;
use mock;

class integer extends atoum\atoum\test
{
    public function test__call()
    {
        $this
            ->assert('Without parent')
                ->if($method = uniqid())
                ->then
                    ->exception(fn () => $this->newTestedInstance->{$method}())
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))

            ->assert('With parent')
                ->given($method = uniqid())
                ->and($args = [uniqid(), uniqid()])
                ->and($node = new mock\atoum\atoum\xml\asserters\node)

                ->if($this->newTestedInstance->setParent($node))
                ->then
                    ->exception(fn () => $this->testedInstance->{$method}(...$args))
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))

                    ->mock($node)
                        ->call('__call')
                            ->withIdenticalArguments($method, $args)->once
        ;
    }

    public function test__get()
    {
        $this
            ->assert('Without parent')
                ->if($asserter = uniqid())
                ->then
                    ->exception(fn () => $this->newTestedInstance->{$asserter})
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

            ->assert('With parent')
                ->given($asserter = uniqid())
                ->and($node = new mock\atoum\atoum\xml\asserters\node)

                ->if($this->newTestedInstance->setParent($node))
                ->then
                    ->exception(fn () => $this->testedInstance->{$asserter})
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

                    ->mock($node)
                        ->call('__get')
                            ->withIdenticalArguments($asserter)->once
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\asserters\integer::class)
        ;
    }

    public function testParent_SetParent()
    {
        $this
            ->assert('No parent defined')
                ->exception(function () {
                    $this->newTestedInstance->parent();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('Parent node is undefined')

            ->assert('Set parent')
                ->if($node = new mock\atoum\atoum\xml\asserters\node)
                ->then
                    ->object($this->newTestedInstance->setParent($node))
                        ->isTestedInstance

                    ->object($this->testedInstance->parent())
                        ->isIdenticalTo($this->testedInstance->parent)
                        ->isNotTestedInstance
                        ->isIdenticalTo($node)
        ;
    }
}
