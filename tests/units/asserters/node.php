<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;
use SimpleXmlElement;

class node extends atoum\atoum\test
{
    public function test__get()
    {
        $this
            ->if($asserter = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$asserter})
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))
        ;
    }

    public function testAttribute()
    {
        $this
            ->assert('Without XML')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->attribute(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without attribute')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($name = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->attribute($name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('No attribute "%s"', $name))

            ->assert('With attribute')
                ->given($name = 'k' . uniqid())
                ->and($value = uniqid())
                ->and($attribute = compact('name', 'value'))
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root ' . $name . '="' . $value . '" />'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attribute($name))
                        ->isInstanceOf(atoum\atoum\xml\asserters\attribute::class)

                    ->array($asserter->getValue())
                        ->isEqualTo($attribute)

            ->assert('With namespace')
                ->given($prefix = 'p' . uniqid())
                ->and($url = uniqid())
                ->and($name = 'k' . uniqid())
                ->and($value = uniqid())
                ->and($attribute = compact('name', 'value', 'prefix', 'url'))
                ->and($tmp = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s" %1$s:%3$s="%4$s" />', [
                    $prefix,
                    $url,
                    $name,
                    $value,
                ]))
                ->and($xml = new SimpleXmlElement($tmp))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attribute($name))
                        ->isInstanceOf(atoum\atoum\xml\asserters\attribute::class)

                    ->array($asserter->getValue())
                        ->isEqualTo($attribute)

                    ->object($asserter = $this->testedInstance->attribute($prefix . ':' . $name))
                        ->isInstanceOf(atoum\atoum\xml\asserters\attribute::class)

                    ->array($asserter->getValue())
                        ->isEqualTo($attribute)
        ;
    }

    public function testAttributes()
    {
        $this
            ->assert('Without XML, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->attributes())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without XML, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->attributes)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With value, called as method')
                ->given($name = 'k' . uniqid())
                ->and($value = uniqid())
                ->and($attributes = compact('name', 'value'))
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root ' . $name . '="' . $value . '" />'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attributes())
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEqualTo([$attributes])

            ->assert('With value, called as property')
                ->given($name = 'k' . uniqid())
                ->and($value = uniqid())
                ->and($attributes = compact('name', 'value'))
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root ' . $name . '="' . $value . '" />'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attributes)
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEqualTo([$attributes])

            ->assert('Without attributes, called as method')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attributes())
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEqualTo([])

            ->assert('Without attributes, called as property')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attributes)
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEqualTo([])

            ->assert('With name')
                ->given($name = 'k' . uniqid())
                ->and($value = uniqid())
                ->and($attributes = compact('name', 'value'))
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root ' . $name . '="' . $value . '" />'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attributes(uniqid()))
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEmpty

                    ->object($asserter = $this->testedInstance->attributes($name))
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEqualTo([$attributes])

            ->assert('With namespace, called as method')
                ->given($prefix = 'p' . uniqid())
                ->and($url = uniqid())
                ->and($name = 'k' . uniqid())
                ->and($value = uniqid())
                ->and($attributes = compact('name', 'value', 'prefix', 'url'))
                ->and($tmp = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s" %1$s:%3$s="%4$s" />', [
                    $prefix,
                    $url,
                    $name,
                    $value,
                ]))
                ->and($xml = new SimpleXmlElement($tmp))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attributes())
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEqualTo([$attributes])

            ->assert('With namespace, called as property')
                ->given($prefix = 'p' . uniqid())
                ->and($url = uniqid())
                ->and($name = 'k' . uniqid())
                ->and($value = uniqid())
                ->and($attributes = compact('name', 'value', 'prefix', 'url'))
                ->and($tmp = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s" %1$s:%3$s="%4$s" />', [
                    $prefix,
                    $url,
                    $name,
                    $value,
                ]))
                ->and($xml = new SimpleXmlElement($tmp))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attributes)
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEqualTo([$attributes])

            ->assert('With namespace, called with name')
                ->given($prefix = 'p' . uniqid())
                ->and($url = uniqid())
                ->and($name = 'k' . uniqid())
                ->and($value = uniqid())
                ->and($attributes = compact('name', 'value', 'prefix', 'url'))
                ->and($tmp = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s" %1$s:%3$s="%4$s" />', [
                    $prefix,
                    $url,
                    $name,
                    $value,
                ]))
                ->and($xml = new SimpleXmlElement($tmp))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->attributes(uniqid()))
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEmpty

                    ->object($asserter = $this->testedInstance->attributes($name))
                        ->isInstanceOf(atoum\atoum\xml\asserters\attributes::class)

                    ->array($asserter->getValue())
                        ->isEqualTo([$attributes])
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\xml\asserters\variable::class)
        ;
    }

    public function testContent()
    {
        $this
            ->assert('Without XML, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->content())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without XML, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->content)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With value, called as method')
                ->given($content = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root>' . $content . '</root>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->content())
                        ->isInstanceOf(atoum\atoum\xml\asserters\content::class)

                    ->string($asserter->getValue())
                        ->isIdenticalTo($content)

            ->assert('With value, called as property')
                ->given($content = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root>' . $content . '</root>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($asserter = $this->testedInstance->content)
                        ->isInstanceOf(atoum\atoum\xml\asserters\content::class)

                    ->string($asserter->getValue())
                        ->isIdenticalTo($content)
        ;
    }

    public function testIsTag()
    {
        $this
            ->assert('Without XML')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isTag(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Bad name')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))
                ->and($name = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isTag($name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Node is "<%s>" not "<%s>"', 'root', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('Bad name, custom message')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))
                ->and($name = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isTag($name, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('Good name')
                ->given($name = 'foo' . uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><' . $name . '><item/></' . $name . '>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isTag($name, $message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testNode()
    {
        $this
            ->assert('Without XML')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->node(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With unknown name')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))
                ->and($name = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->node($name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Node "<%s>" does not exist', $name))

            ->assert('With known name')
                ->given($name = 'item' . uniqid())
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><' . $name . '/></root>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($node = $this->testedInstance->node($name))
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($node->parent)
                        ->isTestedInstance

                    ->object($node->getValue())
                        ->isInstanceOf(SimpleXmlElement::class)
                        ->isNotEqualTo($xml)

                    ->object($node->isTag($name))
        ;
    }

    public function testNodes()
    {
        $this
            ->assert('Without XML, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->nodes())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without XML, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->nodes)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without name, called as method')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($nodes = $this->testedInstance->nodes())
                        ->isInstanceOf(atoum\atoum\xml\asserters\nodes::class)

                    ->object($nodes->parent)
                        ->isTestedInstance

                    ->array($nodes->getValue())
                        ->hasSize(1)
                        ->object[0]
                            ->isInstanceOf(SimpleXmlElement::class)
                            ->isNotEqualTo($xml)

            ->assert('Without name, called as property')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($nodes = $this->testedInstance->nodes)
                        ->isInstanceOf(atoum\atoum\xml\asserters\nodes::class)

                    ->object($nodes->parent)
                        ->isTestedInstance

                    ->array($nodes->getValue())
                        ->isEmpty

            ->assert('With unknown name')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($nodes = $this->testedInstance->nodes(uniqid()))
                        ->isInstanceOf(atoum\atoum\xml\asserters\nodes::class)

                    ->object($nodes->parent)
                        ->isTestedInstance

                    ->array($nodes->getValue())
                        ->isEmpty

            ->assert('With known name')
                ->given($name = 'item' . uniqid())
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><' . $name . '/></root>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($nodes = $this->testedInstance->nodes($name))
                        ->isInstanceOf(atoum\atoum\xml\asserters\nodes::class)

                    ->object($nodes->parent)
                        ->isTestedInstance

                    ->array($nodes->getValue())
                        ->hasSize(1)
                        ->object[0]
                            ->isInstanceOf(SimpleXmlElement::class)
                            ->isNotEqualTo($xml)
        ;
    }

    public function testSetWith()
    {
        $this
            ->assert('Null value')
                ->exception(fn () => $this->newTestedInstance->setWith(null))
                    ->isInstanceOf(atoum\atoum\asserter\exception::class)
                    ->hasMessage('XML is undefined')

            ->assert('With not a XML')
                ->if($value = uniqid())
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid XML', $value))

            ->assert('With SimpleXML instance')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith($xml))
                        ->isTestedInstance

                    ->integer($this->testedInstance->getTest()->getScore()->getAssertionNumber())
                        ->isEqualTo(1)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testXpath()
    {
        $this
            ->assert('Without XML')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->xpath(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With unknown name')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))
                ->and($name = '/' . uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->xpath($name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Path "%s" does not exist', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With custom message')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))
                ->and($name = '/' . uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->xpath($name, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With known name')
                ->given($name = 'item' . uniqid())
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><' . $name . '/></root>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($node = $this->testedInstance->xpath('//' . $name))
                        ->isInstanceOf(atoum\atoum\xml\asserters\nodes::class)

                    ->object($node->parent)
                        ->isTestedInstance

                    ->array($node->getValue())
                        ->hasSize(1)
                        ->object[0]
                            ->isInstanceOf(SimpleXmlElement::class)
                            ->isNotEqualTo($xml)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }
}
