<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;
use atoum\atoum\xml\asserters\nodes as testedClass;
use SimpleXmlElement;

class nodes extends atoum\atoum\test
{
    public function test__call()
    {
        $this
            ->if($method = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$method}())
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))
        ;
    }

    public function test__get()
    {
        $this
            ->if($asserter = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$asserter})
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))
        ;
    }

    public function testAreEmpty_AreNotEmpty()
    {
        $this
            ->assert('Without value, areEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, areEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, areNotEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, areNotEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With non empty element, areEmpty called as method')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($data = [$xml, $xml])
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty element, areEmpty called as property')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($data = [$xml, $xml])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Nodes are not empty, has size %d', count($data)))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty element, areNotEmpty called as method')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($data = [$xml, $xml])
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->areNotEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With non empty element, areNotEmpty called as property')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($data = [$xml, $xml])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->areNotEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, areEmpty called as method')
                ->given($data = [])
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->areEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, areEmpty called as property')
                ->given($data = [])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->areEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, areNotEmpty called as method')
                ->given($data = [])
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With empty element, areNotEmpty called as property')
                ->given($data = [])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Nodes are empty')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\xml\asserters\variable::class)
        ;
    }

    public function testHasSize()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX)))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With negative value')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX) * -1))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value must be positive')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With negative value and custom message')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([$xml]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX) * -1, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With bad size')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($value = random_int(10, PHP_INT_MAX))
                ->and($data = [$xml, $xml])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(vsprintf('Nodes has size %d, expected size %d', [
                            count($data),
                            $value,
                        ]))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With custom message')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([$xml]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(1, PHP_INT_MAX), $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With correct size')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasSize(1))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testIsEmpty_IsNotEmpty()
    {
        $this
            ->assert('Without value, isEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, isEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, isNotEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, isNotEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With non empty element, isEmpty called as method')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($data = [$xml, $xml])
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty element, isEmpty called as property')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($data = [$xml, $xml])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Nodes are not empty, has size %d', count($data)))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty element, isNotEmpty called as method')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($data = [$xml, $xml])
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With non empty element, isNotEmpty called as property')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($data = [$xml, $xml])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, isEmpty called as method')
                ->given($data = [])
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, isEmpty called as property')
                ->given($data = [])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty element, isNotEmpty called as method')
                ->given($data = [])
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With empty element, isNotEmpty called as property')
                ->given($data = [])

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Nodes are empty')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)
        ;
    }

    public function testItem()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->item(random_int(0, PHP_INT_MAX)))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With empty array')
                ->given($position = random_int(1, PHP_INT_MAX))

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->item($position))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('No element at position %d', $position))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With empty array, with custom message')
                ->given($position = random_int(1, PHP_INT_MAX))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->item($position, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With position out of datas')
                ->given($data = [new SimpleXmlElement('<?xml version="1.0"?><root/>')])
                ->and($position = random_int(1, PHP_INT_MAX))

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->item($position))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('No element at position %d', $position))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With position out of datas, with custom message')
                ->given($data = [new SimpleXmlElement('<?xml version="1.0"?><root/>')])
                ->and($position = random_int(1, PHP_INT_MAX))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->item($position, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With valid position')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($item = $this->testedInstance->item(0))
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($item->parent)->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('First')
                ->given($xml1 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($xml2 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($xml3 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([$xml1, $xml2, $xml3]))
                ->then
                    ->object($item = $this->testedInstance->first())
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($item->parent)->isTestedInstance

                    ->object($item = $this->testedInstance->first)
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($item->parent)->isTestedInstance

                ->if($this->newTestedInstance->setWith([]))
                ->then
                    ->exception(fn () => $this->testedInstance->first($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                ->if($this->newTestedInstance->setWith([]))
                ->then
                    ->exception(fn () => $this->testedInstance->first)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('No first element')

            ->assert('Last')
                ->given($xml1 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($xml2 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($xml3 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([$xml1, $xml2, $xml3]))
                ->then
                    ->object($item = $this->testedInstance->last())
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($item->parent)->isTestedInstance
                    ->object($item->getValue())->isIdenticalTo($xml3)

                    ->object($item = $this->testedInstance->last)
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($item->parent)->isTestedInstance
                    ->object($item->getValue())->isIdenticalTo($xml3)

                ->if($this->newTestedInstance->setWith([]))
                ->then
                    ->exception(fn () => $this->testedInstance->last($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                ->if($this->newTestedInstance->setWith([]))
                ->then
                    ->exception(fn () => $this->testedInstance->last)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('No last element')

            ->assert('Next')
                ->given($xml1 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($xml2 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($xml3 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([$xml1, $xml2, $xml3]))
                ->then
                    ->object($item = $this->testedInstance->next())
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($item->parent)->isTestedInstance
                    ->object($item->getValue())->isIdenticalTo($xml1)

                    ->object($item = $this->testedInstance->next)
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($item->parent)->isTestedInstance
                    ->object($item->getValue())->isIdenticalTo($xml2)

                    ->object($item = $this->testedInstance->next)
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

                    ->object($item->parent)->isTestedInstance
                    ->object($item->getValue())->isIdenticalTo($xml3)

                    ->exception(fn () => $this->testedInstance->next($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->exception(fn () => $this->testedInstance->next)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('No next element')
        ;
    }

    public function testNodes()
    {
        $this
            ->assert('Without XML, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->nodes())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without XML, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->nodes)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without name, called as method')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->then
                    ->object($nodes = $this->testedInstance->nodes())
                        ->isInstanceOf(testedClass::class)

                    ->object($nodes->parent)
                        ->isTestedInstance

                    ->array($nodes->getValue())
                        ->hasSize(1)
                        ->object[0]
                            ->isInstanceOf(SimpleXmlElement::class)
                            ->isNotEqualTo($xml)

            ->assert('Without name, called as property')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->then
                    ->object($nodes = $this->testedInstance->nodes)
                        ->isInstanceOf(testedClass::class)

                    ->object($nodes->parent)
                        ->isTestedInstance

                    ->array($nodes->getValue())
                        ->isEmpty

            ->assert('With unknown name')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->then
                    ->object($nodes = $this->testedInstance->nodes(uniqid()))
                        ->isInstanceOf(testedClass::class)

                    ->object($nodes->parent)
                        ->isTestedInstance

                    ->array($nodes->getValue())
                        ->isEmpty

            ->assert('With known name')
                ->given($name = 'item' . uniqid())
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><' . $name . '/></root>'))

                ->if($this->newTestedInstance->setWith([$xml, $xml]))
                ->then
                    ->object($nodes = $this->testedInstance->nodes($name))
                        ->isInstanceOf(testedClass::class)

                    ->object($nodes->parent)
                        ->isTestedInstance

                    ->array($nodes->getValue())
                        ->hasSize(2)
                        ->object[0]
                            ->isInstanceOf(SimpleXmlElement::class)
                            ->isNotEqualTo($xml)

                        ->object[1]
                            ->isInstanceOf(SimpleXmlElement::class)
                            ->isNotEqualTo($xml)
        ;
    }

    public function testSetWith()
    {
        $this
            ->assert('Not an array')
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->exception(fn () => $this->testedInstance->setWith(null))
                    ->isInstanceOf(atoum\atoum\asserter\exception::class)
                    ->hasMessage('Array expected')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('Null value as only parameter')
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->exception(fn () => $this->testedInstance->setWith([null]))
                    ->isInstanceOf(atoum\atoum\asserter\exception::class)
                    ->hasMessage('XML is undefined at position 0')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With not a XML as only parameter')
                ->given($value = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->setWith([$value]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid XML at position 0', $value))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With SimpleXML instance as only parameter')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith([$xml]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('Null value in array')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->setWith([$xml, null]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('XML is undefined at position 1')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With not a XML in array')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($value = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->setWith([$xml, $value]))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid XML at position 1', $value))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With multiple SimpleXML instance in array')
                ->given($xml1 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($xml2 = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith([$xml1, $xml2]))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testSize()
    {
        $this
            ->assert('Without value, method call')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->size())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, property call')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->size)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With value, method call')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->then
                    ->object($this->testedInstance->size())
                        ->isInstanceOf(atoum\atoum\xml\asserters\integer::class)

                    ->integer($this->testedInstance->size()->getValue())
                        ->isEqualTo(1)

            ->assert('With value, property call')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->then
                    ->object($this->testedInstance->size)
                        ->isInstanceOf(atoum\atoum\xml\asserters\integer::class)

                    ->integer($this->testedInstance->size->getValue())
                        ->isEqualTo(1)
        ;
    }

    public function testXpath()
    {
        $this
            ->assert('Without XML')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->xpath(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With unknown name')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))
                ->and($name = uniqid())

                ->if($this->newTestedInstance->setWith([$xml]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->xpath($name))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Path "%s" does not exist', $name))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With custom message')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><item/></root>'))
                ->and($name = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith([$xml]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->xpath($name, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With known name')
                ->given($name = 'item' . uniqid())
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root><' . $name . '/></root>'))

                ->if($this->newTestedInstance->setWith([$xml]))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($node = $this->testedInstance->xpath('//' . $name))
                        ->isInstanceOf(testedClass::class)

                    ->object($node->parent)
                        ->isTestedInstance

                    ->array($node->getValue())
                        ->hasSize(1)
                        ->object[0]
                            ->isInstanceOf(SimpleXmlElement::class)
                            ->isNotEqualTo($xml)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }
}
