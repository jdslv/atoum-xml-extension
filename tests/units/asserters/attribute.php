<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;

class attribute extends atoum\atoum\test
{
    public function test__get()
    {
        $this
            ->if($asserter = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$asserter})
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\xml\asserters\variable::class)
        ;
    }

    public function testContains_NotContains()
    {
        $this
            ->assert('contains, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->contains(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('notContains, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->notContains(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('contains, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->contains($str))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" does not contain "%s"', $value, $str))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notContains, With bad value')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notContains($middle))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" contains "%s"', $value, $middle))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('contains, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->contains($str, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notContains, With bad value and custom message')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notContains($middle, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('contains, With good value')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->contains($middle))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('notContains, With good value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->notContains(uniqid()))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testEndWith_NotEndWith()
    {
        $this
            ->assert('endWith, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->endWith(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('notEndWith, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->notEndWith(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('endWith, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->endWith($str))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" does not end with "%s"', $value, $str))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notEndWith, With bad value')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notEndWith($end))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" ends with "%s"', $value, $end))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('endWith, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->endWith($str, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notEndWith, With bad value and custom message')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notEndWith($end, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('endWith, With good value')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->endWith($end))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('notEndWith, With good value')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->notEndWith($middle))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testHasNamespace_HasNotNamespace()
    {
        $this
            ->assert('hasNamespace, Without value, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasNamespace(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('hasNamespace, Without value, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasNamespace)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('hasNotNamespace, Without value, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotNamespace(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('hasNotNamespace, Without value, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotNamespace)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('hasNamespace, With bad value, called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($str = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasNamespace($str))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute has no namespace %s', $str))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasNamespace, With bad value, called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasNamespace)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Attribute has no namespaces')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasNotNamespace, With bad value, called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotNamespace($prefix))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Attribute has namespace %s', $prefix))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasNotNamespace, With bad value, called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotNamespace)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Attribute has namespaces')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasNamespace, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasNamespace($str, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasNotNamespace, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotNamespace($prefix, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('hasNamespace, With good value, called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasNamespace($prefix))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('hasNamespace, With good value, called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasNamespace)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('hasNotNamespace, With good value, called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasNotNamespace(uniqid()))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('hasNotNamespace, With good value, called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasNotNamespace)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testIsEmpty_IsNotEmpty()
    {
        $this
            ->assert('isEmpty, Without value, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('isEmpty, Without value, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('isNotEmpty, Without value, called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('isNotEmpty, Without value, called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('isEmpty, With bad value, called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isEmpty, With bad value, called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value is not empty')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isNotEmpty, With bad value, called as method')
                ->given($name = uniqid())
                ->and($value = '')
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isNotEmpty, With bad value, called as property')
                ->given($name = uniqid())
                ->and($value = '')
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value is empty')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isEmpty, With good value, called as method')
                ->given($name = uniqid())
                ->and($value = '')
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEmpty())
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('isEmpty, With good value, called as property')
                ->given($name = uniqid())
                ->and($value = '')
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('isNotEmpty, With good value, called as method')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEmpty())
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('isNotEmpty, With good value, called as property')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testIsEqualTo_IsNotEqualTo()
    {
        $this
            ->assert('isEqualTo, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEqualTo(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('isNotEqualTo, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEqualTo(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('isEqualTo, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEqualTo($str))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not equal to "%s"', $value, $str))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isNotEqualTo, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEqualTo($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Value is equal to "%s"', $value))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isEqualTo, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEqualTo($str, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isNotEqualTo, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEqualTo($value, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isEqualTo, With good value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEqualTo($value))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('isNotEqualTo, With good value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEqualTo(uniqid()))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testIsIdenticalTo_IsNotIdenticalTo()
    {
        $this
            ->assert('isIdenticalTo, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isIdenticalTo(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('isNotIdenticalTo, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotIdenticalTo(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('isIdenticalTo, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isIdenticalTo($str))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not identical to "%s"', $value, $str))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isNotIdenticalTo, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotIdenticalTo($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Value is identical to "%s"', $value))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isIdenticalTo, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isIdenticalTo($str, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isNotIdenticalTo, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotIdenticalTo($value, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('isIdenticalTo, With good value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isIdenticalTo($value))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('isNotIdenticalTo, With good value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotIdenticalTo(uniqid()))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testMatches_NotMatches()
    {
        $this
            ->assert('matches, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->matches(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('notMatches, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->notMatches(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('matches, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($pattern = '/^\d+$/')

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->matches($pattern))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" does not match "%s"', $value, $pattern))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notMatches, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($pattern = '/.+/')

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notMatches($pattern))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" matches "%s"', $value, $pattern))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('matches, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($pattern = '/^\d+$/')
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->matches($pattern, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notMatches, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($pattern = '/.+/')
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notMatches($pattern, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('matches, With good value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($pattern = '/.+/')

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->matches($pattern))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('notMatches, With good value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($pattern = '/^\d+$/')

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->notMatches($pattern))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testSetWith()
    {
        $this
            ->assert('Null value')
                ->exception(fn () => $this->newTestedInstance->setWith(null))
                    ->isInstanceOf(atoum\atoum\asserter\exception::class)
                    ->hasMessage('Value is undefined')

            ->assert('With invalid data')
                ->given($value = random_int(0, PHP_INT_MAX))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value must be an array')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With missing name')
                ->given($value = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(function () use ($value) {
                        $this->testedInstance->setWith(compact('value'));
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Name is missing in the attribute')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With missing value')
                ->given($name = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(function () use ($name) {
                        $this->testedInstance->setWith(compact('name'));
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value is missing in the attribute')

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With valid attribute data')
                ->given($name = uniqid())
                ->and($value = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith(compact('name', 'value')))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With valid namespaced attribute data')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith(compact('name', 'value', 'prefix')))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testStartWith_NotStartWith()
    {
        $this
            ->assert('startWith, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->startWith(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('notStartWith, Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->notStartWith(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('startWith, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->startWith($str))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" does not start with "%s"', $value, $str))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notStartWith, With bad value')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notStartWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" starts with "%s"', $value, $value))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('startWith, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($str = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->startWith($str, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('notStartWith, With bad value and custom message')
                ->given($name = uniqid())
                ->and($value = uniqid())
                ->and($prefix = uniqid())
                ->and($url = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notStartWith($value, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('startWith, With good value')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->startWith($start))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('notStartWith, With good value')
                ->given($name = uniqid())
                ->and($start = uniqid())
                ->and($middle = uniqid())
                ->and($end = uniqid())
                ->and($value = $start . $middle . $end)
                ->and($prefix = uniqid())
                ->and($url = uniqid())

                ->if($this->newTestedInstance->setWith(compact('name', 'prefix', 'url', 'value')))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->notStartWith($middle))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }
}
