<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;
use mock;

class content extends atoum\atoum\test
{
    public function test__call()
    {
        $this
            ->assert('Without parent')
                ->if($method = uniqid())
                ->then
                    ->exception(fn () => $this->newTestedInstance->{$method}())
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))

            ->assert('With parent')
                ->given($method = uniqid())
                ->and($args = [uniqid(), uniqid()])
                ->and($node = new mock\atoum\atoum\xml\asserters\node)

                ->if($this->newTestedInstance->setParent($node))
                ->then
                    ->exception(fn () => $this->testedInstance->{$method}(...$args))
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))

                    ->mock($node)
                        ->call('__call')
                            ->withIdenticalArguments($method, $args)->once
        ;
    }

    public function test__get()
    {
        $this
            ->assert('Without parent')
                ->if($asserter = uniqid())
                ->then
                    ->exception(fn () => $this->newTestedInstance->{$asserter})
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

            ->assert('With parent')
                ->given($asserter = uniqid())
                ->and($node = new mock\atoum\atoum\xml\asserters\node)

                ->if($this->newTestedInstance->setParent($node))
                ->then
                    ->exception(fn () => $this->testedInstance->{$asserter})
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

                    ->mock($node)
                        ->call('__get')
                            ->withIdenticalArguments($asserter)->once
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\asserters\phpString::class)
        ;
    }

    public function testIsEmpty_IsNotEmpty()
    {
        $this
            ->assert('Without value, isEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, isEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, isNotEmpty called as method')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('Without value, isNotEmpty called as property')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('Value is undefined')

            ->assert('With non empty string, isEmpty called as method')
                ->given($data = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty string, isEmpty called as property')
                ->given($data = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Content "%s" is not empty, has size %d', $data, strlen($data)))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With non empty string, isNotEmpty called as method')
                ->given($data = uniqid())
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With non empty string, isNotEmpty called as property')
                ->given($data = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isNotEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty string, isEmpty called as method')
                ->given($data = '')
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEmpty($message))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty string, isEmpty called as property')
                ->given($data = '')

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->isEmpty)
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With empty string, isNotEmpty called as method')
                ->given($data = '')
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With empty string, isNotEmpty called as property')
                ->given($data = '')

                ->if($this->newTestedInstance->setWith($data))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Content is empty, has size %d', strlen($data)))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)
        ;
    }

    public function testParent_SetParent()
    {
        $this
            ->assert('No parent defined')
                ->exception(fn () => $this->newTestedInstance->parent())
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('Parent node is undefined')

            ->assert('Set parent')
                ->if($node = new mock\atoum\atoum\xml\asserters\node)
                ->then
                    ->object($this->newTestedInstance->setParent($node))
                        ->isTestedInstance

                    ->object($this->testedInstance->parent())
                        ->isIdenticalTo($this->testedInstance->parent)
                        ->isNotTestedInstance
                        ->isIdenticalTo($node)
        ;
    }
}
