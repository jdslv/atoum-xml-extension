<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units\asserters;

use atoum;
use SimpleXmlElement;

class namespaces extends atoum\atoum\test
{
    public function test__call()
    {
        $this
            ->if($method = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$method}())
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))
        ;
    }

    public function test__get()
    {
        $this
            ->if($asserter = uniqid())
            ->then
                ->exception(fn () => $this->newTestedInstance->{$asserter})
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))
        ;
    }

    public function testAreEmpty_AreNotEmpty()
    {
        $this
            ->assert('`areEmpty` called as a method, without value')
                ->given($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('`areEmpty` called as a property, without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('`areNotEmpty` called as a method, without value')
                ->given($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('`areNotEmpty` called as a property, without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('`areEmpty` called as a method on non empty namespaces')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('`areEmpty` called as a property on non empty namespaces')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->areEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespaces are not empty')

            ->assert('`areNotEmpty` called as a method on non empty namespaces')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->areNotEmpty($message))
                        ->isTestedInstance

            ->assert('`areNotEmpty` called as a property on non empty namespaces')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->areNotEmpty)
                        ->isTestedInstance

            ->assert('`areEmpty` called as a method on empty namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->areEmpty())
                        ->isTestedInstance

            ->assert('`areEmpty` called as a property on non empty namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->areEmpty)
                        ->isTestedInstance

            ->assert('`areNotEmpty` called as a method on non empty namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('`areNotEmpty` called as a property on non empty namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->areNotEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespaces are empty')
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\xml\asserters\variable::class)
        ;
    }

    public function testContains()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->contains(uniqid(), uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($prefix = uniqid())
                ->and($uri = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->contains($prefix, $uri))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('XML does not contains namespace "%s"', $prefix))

            ->assert('With a specific message')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->contains(uniqid(), uniqid(), $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('With good prefix and bad URI, default message')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($tmp = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s" />', [
                    $prefix,
                    uniqid(),
                ]))
                ->and($xml = new SimpleXmlElement($tmp))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->contains($prefix, $uri))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" namespace does not match the URI "%s"', $prefix, $uri))

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With good prefix and bad URI, custom message')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($tmp = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s" />', [
                    $prefix,
                    uniqid(),
                ]))
                ->and($xml = new SimpleXmlElement($tmp))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->contains($prefix, $uri, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With good prefix and good URI')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->contains($prefix, $uri))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(2)
        ;
    }

    public function testHasNotPrefix()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotPrefix(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($prefix = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasNotPrefix($prefix))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With good prefix')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotPrefix($prefix))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('XML has a namespace prefix "%s"', $prefix))

            ->assert('With custom message')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotPrefix($prefix, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)
        ;
    }

    public function testHasNotUri()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotUri(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($uri = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasNotUri($uri))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With good URI')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotUri($uri))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('XML has a namespace URI "%s"', $uri))

            ->assert('With custom message')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasNotUri($uri, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)
        ;
    }

    public function testHasPrefix()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasPrefix(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($prefix = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasPrefix($prefix))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('XML has no namespace prefix "%s"', $prefix))

            ->assert('With a specific message')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasPrefix(uniqid(), $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('With good prefix')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasPrefix($prefix))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testHasSize()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX)))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With negative value')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX) * -1))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Value must be positive')

            ->assert('With negative value and custom message')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize(random_int(0, PHP_INT_MAX) * -1, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('With bad size')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($size = random_int(2, PHP_INT_MAX))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize($size))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('Namespaces has size %d, expected size %d', 1, $size))

            ->assert('With custom message')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($size = random_int(2, PHP_INT_MAX))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasSize($size, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('With correct size')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->hasSize(1))
                        ->isTestedInstance
        ;
    }

    public function testHasUri()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->hasUri(uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($uri = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasUri($uri))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('XML has no namespace URI "%s"', $uri))

            ->assert('With a specific message')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->hasUri(uniqid(), $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('With good URI')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->hasUri($uri))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    public function testIsEmpty_IsNotEmpty()
    {
        $this
            ->assert('`isEmpty` called as a method, without value')
                ->given($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('`isEmpty` called as a property, without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('`isNotEmpty` called as a method, without value')
                ->given($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('`isNotEmpty` called as a property, without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('`isEmpty` called as a method on non empty namespaces')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('`isEmpty` called as a property on non empty namespaces')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->isEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespaces are not empty')

            ->assert('`isNotEmpty` called as a method on non empty namespaces')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->isNotEmpty($message))
                        ->isTestedInstance

            ->assert('`isNotEmpty` called as a property on non empty namespaces')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->isNotEmpty)
                        ->isTestedInstance

            ->assert('`isEmpty` called as a method on empty namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->isEmpty())
                        ->isTestedInstance

            ->assert('`isEmpty` called as a property on non empty namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->isEmpty)
                        ->isTestedInstance

            ->assert('`isNotEmpty` called as a method on non empty namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty($message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

            ->assert('`isNotEmpty` called as a property on non empty namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->exception(fn () => $this->testedInstance->isNotEmpty)
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('Namespaces are empty')
        ;
    }

    public function testNotContains()
    {
        $this
            ->assert('Without value')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->notContains(uniqid(), uniqid()))
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without namespaces')
                ->given($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->and($prefix = uniqid())
                ->and($uri = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->notContains($prefix, $uri))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(2)

            ->assert('With bad prefix and good URI, default message')
                ->given($prefix = uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:foo="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notContains($prefix, $uri))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('XML contains namespace URI "%s"', $uri))

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With bad prefix and good URI, custom message')
                ->given($prefix = uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:foo="' . $uri . '"/>'))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notContains($prefix, $uri, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(2)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With good prefix and bad URI, default message')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($tmp = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s" />', [
                    $prefix,
                    uniqid(),
                ]))
                ->and($xml = new SimpleXmlElement($tmp))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notContains($prefix, $uri))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('XML contains namespace "%s"', $prefix))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With good prefix and bad URI, custom message')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($tmp = vsprintf('<?xml version="1.0"?><root xmlns:%1$s="%2$s" />', [
                    $prefix,
                    uniqid(),
                ]))
                ->and($xml = new SimpleXmlElement($tmp))
                ->and($message = uniqid())

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notContains($prefix, $uri, $message))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage($message)

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With good prefix and good URI')
                ->given($prefix = 'foo')
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(fn () => $this->testedInstance->notContains($prefix, $uri))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('XML contains namespace "%s"', $prefix))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)
        ;
    }

    public function testSetWith()
    {
        $this
            ->assert('Null value')
                ->exception(fn () => $this->newTestedInstance->setWith(null))
                    ->isInstanceOf(atoum\atoum\asserter\exception::class)
                    ->hasMessage('XML is undefined')

            ->assert('With not a XML')
                ->if($value = uniqid())
                ->then
                    ->exception(fn () => $this->newTestedInstance->setWith($value))
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid XML', $value))

            ->assert('With SimpleXML instance')
                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root/>'))
                ->then
                    ->object($this->testedInstance->setWith($xml))
                        ->isTestedInstance

                    ->integer($this->testedInstance->getTest()->getScore()->getAssertionNumber())
                        ->isEqualTo(1)
        ;
    }

    public function testSize()
    {
        $this
            ->assert('Without value, method call')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->size())
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('Without value, property call')
                ->if($this->newTestedInstance)
                ->then
                    ->exception(fn () => $this->testedInstance->size)
                        ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                        ->hasMessage('XML is undefined')

            ->assert('With value, method call')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->size())
                        ->isInstanceOf(atoum\atoum\xml\asserters\integer::class)

                    ->integer($this->testedInstance->size()->getValue())
                        ->isEqualTo(1)

            ->assert('With value, property call')
                ->given($prefix = 'a' . uniqid())
                ->and($uri = uniqid())
                ->and($xml = new SimpleXmlElement('<?xml version="1.0"?><root xmlns:' . $prefix . '="' . $uri . '"/>'))

                ->if($this->newTestedInstance->setWith($xml))
                ->then
                    ->object($this->testedInstance->size)
                        ->isInstanceOf(atoum\atoum\xml\asserters\integer::class)

                    ->integer($this->testedInstance->size->getValue())
                        ->isEqualTo(1)
        ;
    }
}
