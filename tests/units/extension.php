<?php

declare(strict_types=1);

namespace atoum\atoum\xml\tests\units;

use atoum;
use mock;

class extension extends atoum\atoum\test
{
    public function test__construct()
    {
        $this
            ->given($runner = new mock\atoum\atoum\runner)
            ->and($this->calling($runner)->addTestsFromDirectory->isFluent)
            ->and($parser = new mock\atoum\atoum\script\arguments\parser)

            ->if($script = new atoum\atoum\scripts\runner(uniqid()))
            ->and($script->setArgumentsParser($parser))
            ->and($script->setRunner($runner))

            ->if($configurator = new mock\atoum\atoum\configurator($script))
            ->then
                ->object($this->newTestedInstance)

            ->if($this->resetMock($parser))
            ->then
                ->object($this->newTestedInstance($configurator))
                ->mock($parser)
                    ->call('addHandler')->once

                ->object($parser->triggerHandlers('--test-ext', [], $script))
                    ->isIdenticalTo($parser)

                ->mock($runner)
                    ->call('addTestsFromDirectory')
                        ->withArguments(realpath(__DIR__ . '/../functionals'))->once
                        ->withArguments(realpath(__DIR__ . '/../units'))->once
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->hasInterface(atoum\atoum\extension::class)
        ;
    }

    public function testHandleEvent()
    {
        $this
            ->given($observable = new mock\atoum\atoum\observable)
            ->and($this->resetMock($observable))

            ->if($event = uniqid())
            ->then
                ->variable($this->newTestedInstance->handleEvent($event, $observable))
                    ->isNull

                ->mock($observable)->wasNotCalled
        ;
    }

    public function testSetRunner()
    {
        $this
            ->given($runner = new mock\atoum\atoum\runner)
            ->and($this->resetMock($runner))
            ->then
                ->object($this->newTestedInstance->setRunner($runner))
                    ->isTestedInstance

                ->mock($runner)->wasNotCalled
        ;
    }

    public function testSetTest()
    {
        $this
            ->assert('Add handler')
                ->if($this->newTestedInstance)
                ->and($manager = new mock\atoum\atoum\test\assertion\manager)
                ->and($test = new mock\atoum\atoum\test)
                ->and($test->setAssertionManager($manager))
                ->then
                    ->object($this->testedInstance->setTest($test))
                        ->isTestedInstance

                    ->mock($manager)
                        ->call('setHandler')
                            ->withArguments('xml')->once

            ->assert('With value')
                ->given($test = new static)
                ->and($xml = '<?xml version="1.0" ?><root></root>')

                ->if($this->newTestedInstance->setTest($test))
                ->then
                    ->object($test->xml($xml))
                        ->isInstanceOf(atoum\atoum\xml\asserters\node::class)

            ->assert('Without value')
                ->given($test = new static)

                ->if($this->newTestedInstance->setTest($test))
                ->then
                    ->exception(function () use ($test) {
                        $test->xml;
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('XML is undefined')
        ;
    }
}
