<?php

namespace atoum\atoum\xml\tests\functionals\asserters;

use atoum\atoum\xml\tests\functionals;

class xml extends functionals\test
{
    public function testNamespaces()
    {
        $this
            ->xml($this->resource('namespace.xml'))
                ->namespace('m', 'http://purl.org/atom/ns#')
                    ->notExists
                ->namespace('dc', 'http://purl.org/dc/elements/1.1/')
                    ->isUsed
                ->namespace('atom', 'http://purl.org/atom/ns#')
                    ->isUsed
        ;
    }

    public function testSimpleParsing()
    {
        $this
            ->xml($this->resource('simple.xml'))
                ->namespace('m', 'http://example.com')
                    ->isUsed
                ->nodes
                    ->hasSize(2)
                    ->nodes
                        ->size
                            ->isEqualTo(1)
                ->root
                    ->nodes(uniqid())
                        ->isEmpty
                ->root
                    ->isTag('root')
                    ->nodes('node')
                        ->isNotEmpty
                        ->hasSize(2)
                        ->first
                            ->nodes
                                ->isEmpty
                            ->attributes
                                ->hasSize(2)
                                ->string['attr1']
                                    ->isIdenticalTo('value 1')
                                    ->hasNotNamespace
                                ->string['m:attr2']
                                    ->isEqualTo('value 2')
                                ->string['attr2']
                                    ->startWith('value')
                                    ->endWith('2')
                                    ->hasNamespace('m')
                            ->content
                                ->isIdenticalTo('Node content')
                        ->next
                            ->nodes
                                ->hasSize(1)
                                ->item(0)
                                    ->isTag('subnode')
                                    ->content
                                        ->isEmpty
        ;
    }

    public function testSVG()
    {
        $this
            ->xml($this->resource('image.svg'))
                ->isTag('svg')

                ->attributes
                    ->hasKeys(['height', 'version', 'width'])
                    ->string['height']->isEqualTo(200)
                    ->string['version']->isEqualTo(1.1)
                    ->string['width']->isEqualTo(300)

                ->nodes
                    ->hasSize(3)

                    ->first
                        ->isTag('rect')
                        ->attributes
                            ->string['fill']->isEqualTo('green')
                            ->string['x']->isEqualTo(0)
                            ->string['y']->isEqualTo(70)
                            ->string['width']->isEqualTo(100)
                            ->string['height']->isEqualTo(80)
                        ->nodes
                            ->isEmpty
                        ->content
                            ->isEmpty
                    ->next
                        ->isTag('line')
                        ->attributes
                            ->string['stroke']->isEqualTo('red')
                            ->string['x1']->isEqualTo(5)
                            ->string['y1']->isEqualTo(5)
                            ->string['x2']->isEqualTo(250)
                            ->string['y2']->isEqualTo(95)
                        ->nodes
                            ->isEmpty
                        ->content
                            ->isEmpty
                    ->next
                        ->isTag('circle')
                        ->attributes
                            ->string['fill']->isEqualTo('blue')
                            ->string['cx']->isEqualTo(90)
                            ->string['cy']->isEqualTo(80)
                            ->string['r']->isEqualTo(50)
                        ->nodes
                            ->isEmpty
                        ->content
                            ->isEmpty
        ;
    }
}
