<?php

namespace atoum\atoum\xml\tests\functionals;

use atoum\atoum;
use atoum\atoum\adapter;
use atoum\atoum\annotations\extractor;
use atoum\atoum\asserter\generator;
use atoum\atoum\exceptions\runtime;
use atoum\atoum\test\assertion\manager;
use atoum\atoum\tools\variable\analyzer;
use closure;

class test extends atoum\test
{
    public function __construct(
        ?adapter $adapter = null,
        ?extractor $extractor = null,
        ?generator $generator = null,
        ?manager $manager = null,
        ?closure $reflectionClassFactory = null,
        ?closure $phpExtensionFactory = null,
        ?analyzer $analyzer = null,
    ) {
        parent::__construct(
            $adapter,
            $extractor,
            $generator,
            $manager,
            $reflectionClassFactory,
            $phpExtensionFactory,
            $analyzer,
        );

        $this->getAsserterGenerator()->addNamespace('atoum\atoum\xml\asserters');
        $this->setTestNamespace('\tests\functionals');
    }

    public function resource(string $name): string
    {
        $path = realpath(__DIR__ . '/../resources/' . $name);

        if (!$path) {
            throw new runtime($this->locale->_('"%s" is not a valid resource.', $name));
        }

        return file_get_contents($path);
    }
}
