<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

/**
 * Assertion dedicated to a single attribute.
 */
class attribute extends variable
{
    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'hasnamespace':
            case 'hasnotnamespace':
            case 'isempty':
            case 'isnotempty':
                return $this->{$asserter}();

            default:
                return parent::__get($asserter);
        }
    }

    /**
     * Checks that the attribute value contain a given string.
     */
    public function contains(string $value, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($this->adapter->str_contains($current, $value)) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? '"%s" does not contain "%s"', $current, $value));
        }

        return $this;
    }

    /**
     * Checks that the attribute value ends with a given string.
     */
    public function endWith(string $value, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($this->adapter->str_ends_with($current, $value)) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? '"%s" does not end with "%s"', $current, $value));
        }

        return $this;
    }

    /**
     * Checks that the attribute name have a namespace.
     */
    public function hasNamespace(?string $prefix = null, ?string $message = null): static
    {
        $ns = $this->valueIsSet()->value['prefix'] ?? '';

        if ($prefix && $prefix !== $ns) {
            $this->fail($this->_($message ?? 'Attribute has no namespace %s', $prefix));
        } elseif (!$prefix && !$ns) {
            $this->fail($message ?? 'Attribute has no namespaces');
        }

        $this->pass();

        return $this;
    }

    /**
     * Checks that the attribute name have not a namespace.
     */
    public function hasNotNamespace(?string $prefix = null, ?string $message = null): static
    {
        $ns = $this->valueIsSet()->value['prefix'] ?? '';

        if ($prefix && $prefix === $ns) {
            $this->fail($this->_($message ?? 'Attribute has namespace %s', $prefix));
        } elseif (!$prefix && $ns) {
            $this->fail($message ?? 'Attribute has namespaces');
        }

        $this->pass();

        return $this;
    }

    /**
     * Checks that the attribute value is empty.
     */
    public function isEmpty(?string $message = null): static
    {
        return $this->isEqualTo('', $message ?? 'Value is not empty');
    }

    /**
     * Checks that the attribute value is equal to a given string.
     *
     * @param mixed $value
     * @param string|null $message
     */
    public function isEqualTo($value, $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($current == $value) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? '"%s" is not equal to "%s"', $current, $value));
        }

        return $this;
    }

    /**
     * Checks that the attribute value is identical to a given string.
     *
     * @param mixed $value
     * @param string|null $message
     */
    public function isIdenticalTo($value, $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($current === $value) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? '"%s" is not identical to "%s"', $current, $value));
        }

        return $this;
    }

    /**
     * Checks that the attribute value is not empty.
     */
    public function isNotEmpty(?string $message = null): static
    {
        return $this->isNotEqualTo('', $message ?? 'Value is empty');
    }

    /**
     * Checks that the attribute value is not equal to a given string.
     *
     * @param mixed $value
     * @param string|null $message
     */
    public function isNotEqualTo($value, $message = null): static
    {
        if ($this->valueIsSet()->value['value'] == $value) {
            $this->fail($this->_($message ?? 'Value is equal to "%s"', $value));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks that the attribute value is not identical to a given string.
     *
     * @param mixed $value
     * @param string|null $message
     */
    public function isNotIdenticalTo($value, $message = null): static
    {
        if ($this->valueIsSet()->value['value'] === $value) {
            $this->fail($this->_($message ?? 'Value is identical to "%s"', $value));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks that the attribute value match a pattern.
     */
    public function matches(string $pattern, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($this->adapter->preg_match($pattern, $current) === 1) {
            $this->pass();
        } else {
            $this->fail($message ?: $this->_('"%s" does not match "%s"', $current, $pattern));
        }

        return $this;
    }

    /**
     * Checks that the attribute value does not contain a given string.
     */
    public function notContains(string $value, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($this->adapter->str_contains($current, $value)) {
            $this->fail($this->_($message ?? '"%s" contains "%s"', $current, $value));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks that the attribute value does not end with a given string.
     */
    public function notEndWith(string $value, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($this->adapter->str_ends_with($current, $value)) {
            $this->fail($this->_($message ?? '"%s" ends with "%s"', $current, $value));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks that the attribute value does not match a pattern.
     */
    public function notMatches(string $pattern, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($this->adapter->preg_match($pattern, $current) === 0) {
            $this->pass();
        } else {
            $this->fail($message ?: $this->_('"%s" matches "%s"', $current, $pattern));
        }

        return $this;
    }

    /**
     * Checks that the attribute value does not start with a given string.
     */
    public function notStartWith(string $value, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($this->adapter->str_starts_with($current, $value)) {
            $this->fail($this->_($message ?? '"%s" starts with "%s"', $current, $value));
        } else {
            $this->pass();
        }

        return $this;
    }

    public function setWith($value): static
    {
        if ($value === null) {
            $this->fail($this->_('Value is undefined'));
        } elseif (!$this->adapter->is_array($value)) {
            $this->fail($this->_('Value must be an array'));
        } elseif (!$this->adapter->array_key_exists('name', $value)) {
            $this->fail($this->_('Name is missing in the attribute'));
        } elseif (!$this->adapter->array_key_exists('value', $value)) {
            $this->fail($this->_('Value is missing in the attribute'));
        } else {
            $this->pass();
        }

        return parent::setWith($value);
    }

    /**
     * Checks that the attribute value starts with a given string.
     */
    public function startWith(string $value, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value['value'];

        if ($this->adapter->str_starts_with($current, $value)) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? '"%s" does not start with "%s"', $current, $value));
        }

        return $this;
    }

    /**
     * @param string $message
     */
    protected function valueIsSet($message = 'Value is undefined'): static
    {
        return parent::valueIsSet($message);
    }
}
