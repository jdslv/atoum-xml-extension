<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

/**
 * Assertion dedicated to a single namespace.
 */
class xmlNamespace extends variable
{
    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'exists':
            case 'isnotused':
            case 'isused':
            case 'notexists':
                return $this->{$asserter}();

            default:
                return parent::__get($asserter);
        }
    }

    /**
     * Check if the namespace exists in the document.
     */
    public function exists(string $message = 'Namespace does not exist'): static
    {
        if ($this->valueIsSet()->adapter->array_key_exists('prefix', $this->value)) {
            $this->pass();
        } else {
            $this->fail($message);
        }

        return $this;
    }

    /**
     * Check if the namespace is not used in the document.
     */
    public function isNotUsed(string $message = 'Namespace is used'): static
    {
        if ($this->valueIsSet()->adapter->array_key_exists('isUsed', $this->value) && $this->value['isUsed']) {
            $this->fail($message);
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Check if the namespace is used in the document.
     */
    public function isUsed(string $message = 'Namespace is not used'): static
    {
        if ($this->valueIsSet()->adapter->array_key_exists('isUsed', $this->value) && $this->value['isUsed']) {
            $this->pass();
        } else {
            $this->fail($message);
        }

        return $this;
    }

    /**
     * Check if the namespace does not exist in the document.
     */
    public function notExists(string $message = 'Namespace does exist'): static
    {
        if ($this->valueIsSet()->adapter->array_key_exists('prefix', $this->value)) {
            $this->fail($message);
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * @param array $value
     */
    public function setWith($value): static
    {
        parent::setWith($value);

        if ($this->adapter->is_array($value)) {
            $hasPrefix = $this->adapter->array_key_exists('prefix', $value);
            $hasUri = $this->adapter->array_key_exists('uri', $value);
            $hasIsUsed = $this->adapter->array_key_exists('isUsed', $value);

            if ($hasPrefix xor $hasUri) {
                $message = '"%s" must have `prefix` and `uri` keys';

                $this->fail($this->_($message, $this->adapter->var_export($value, true)));
            } elseif ($hasIsUsed && !$this->adapter->is_bool($value['isUsed'])) {
                $this->fail($this->_('`isUsed` must be a boolean'));
            } else {
                $this->pass();
            }
        } else {
            $this->fail($this->_('"%s" must be an array', $value));
        }

        return $this;
    }
}
