<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

use atoum\atoum\xml\asserters\integer as integerAsserter;
use SimpleXmlElement;

/**
 * Assertion dedicated to namespaces.
 */
class namespaces extends variable
{
    /**
     * @param string $method
     * @param mixed $arguments
     */
    public function __call($method, $arguments)
    {
        switch (strtolower($method)) {
            case 'areempty':
                return $this->isEmpty(...$arguments);

            case 'arenotempty':
                return $this->isNotEmpty(...$arguments);
        }

        return parent::__call($method, $arguments);
    }

    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'areempty':
            case 'arenotempty':
            case 'isempty':
            case 'isnotempty':
            case 'size':
                return $this->{$asserter}();
        }

        return parent::__get($asserter);
    }

    /**
     * Checks that namespaces contain a given string.
     */
    public function contains(string $prefix, string $uri, ?string $message = null): static
    {
        return $this
            ->hasPrefix($prefix, $message ?? 'XML does not contains namespace "%s"')
            ->hasUri($uri, $message ?? $this->_('"%s" namespace does not match the URI "%s"', $prefix, '%s'))
        ;
    }

    /**
     * Checks that namespaces have not a given prefix.
     */
    public function hasNotPrefix(string $prefix, ?string $message = null): static
    {
        $ns = $this->valueIsSet()->value->getDocNamespaces();

        if ($this->adapter->array_key_exists($prefix, $ns)) {
            $this->fail($this->_($message ?? 'XML has a namespace prefix "%s"', $prefix));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks that namespaces have not a given URI.
     */
    public function hasNotUri(string $uri, ?string $message = null): static
    {
        $ns = $this->valueIsSet()->value->getDocNamespaces();

        if ($this->adapter->in_array($uri, $ns, true)) {
            $this->fail($this->_($message ?? 'XML has a namespace URI "%s"', $uri));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks that namespaces have a given prefix.
     */
    public function hasPrefix(string $prefix, ?string $message = null): static
    {
        $ns = $this->valueIsSet()->value->getDocNamespaces();

        if ($this->adapter->array_key_exists($prefix, $ns)) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'XML has no namespace prefix "%s"', $prefix));
        }

        return $this;
    }

    /**
     * Checks that namespaces has a given size.
     */
    public function hasSize(int $size, ?string $message = null): static
    {
        $count = count($this->valueIsSet()->value->getDocNamespaces());

        if ($size < 0) {
            $this->fail($this->_($message ?? 'Value must be positive'));
        } elseif ($count === $size) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'Namespaces has size %d, expected size %d', $count, $size));
        }

        return $this;
    }

    /**
     * Checks that namespaces have a given URI.
     */
    public function hasUri(string $uri, ?string $message = null): static
    {
        $ns = $this->valueIsSet()->value->getDocNamespaces();

        if ($this->adapter->in_array($uri, $ns, true)) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'XML has no namespace URI "%s"', $uri));
        }

        return $this;
    }

    /**
     * Checks that the namespace list is empty.
     */
    public function isEmpty(string $message = 'Namespaces are not empty'): static
    {
        return $this->valueIsSet()->hasSize(0, $message);
    }

    /**
     * Checks that the namespace list is not empty.
     */
    public function isNotEmpty(string $message = 'Namespaces are empty'): static
    {
        $count = count($this->valueIsSet()->value->getDocNamespaces());

        if ($count === 0) {
            $this->fail($this->_($message));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks that namespaces do not contain a given string.
     */
    public function notContains(string $prefix, string $uri, ?string $message = null): static
    {
        return $this
            ->hasNotPrefix($prefix, $message ?? 'XML contains namespace "%s"')
            ->hasNotUri($uri, $message ?? 'XML contains namespace URI "%s"')
        ;
    }

    /**
     * @param array $value
     */
    public function setWith($value): static
    {
        parent::setWith($value);

        if ($value instanceof SimpleXmlElement) {
            $this->pass();
        } elseif ($value === null) {
            $this->fail('XML is undefined');
        } else {
            $this->fail($this->_('"%s" is not a valid XML', $value));
        }

        return $this;
    }

    /**
     * Return an `integer` asserter based attributes size.
     */
    public function size(): integerAsserter
    {
        $count = count($this->valueIsSet()->value->getDocNamespaces());

        return $this->generator->__call(integerAsserter::class, [$count])->setParent($this);
    }
}
