<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

use atoum\atoum\asserters;
use atoum\atoum\xml\traits;

/**
 * Asserter dedicated to node's content.
 */
class content extends asserters\phpString
{
    use traits\parentTrait;

    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'isempty':
            case 'isnotempty':
            case 'parent':
                return $this->{$asserter}();

            default:
                if ($this->parent) {
                    return $this->parent->__get($asserter);
                }

                return parent::__get($asserter);
        }
    }

    /**
     * Checks if the content is empty.
     *
     * @param string|null $message
     */
    public function isEmpty($message = null): static
    {
        $count = strlen($this->valueIsSet()->value);

        if ($count) {
            $this->fail($this->_($message ?? 'Content "%s" is not empty, has size %d', $this->value, $count));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks if the content is not empty.
     *
     * @param string|null $message
     */
    public function isNotEmpty($message = null): static
    {
        $count = strlen($this->valueIsSet()->value);

        if (!$count) {
            $this->fail($this->_($message ?? 'Content is empty, has size %d', $count));
        } else {
            $this->pass();
        }

        return $this;
    }
}
