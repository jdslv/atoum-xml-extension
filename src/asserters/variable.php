<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

use atoum;
use atoum\atoum\adapter;
use atoum\atoum\asserter\generator;
use atoum\atoum\locale;
use atoum\atoum\tools\variable\analyzer;
use atoum\atoum\xml\traits;

class variable extends atoum\atoum\asserters\variable
{
    use traits\parentTrait;

    protected adapter $adapter;

    public function __construct(
        ?generator $generator = null,
        ?analyzer $analyzer = null,
        ?locale $locale = null,
        ?adapter $adapter = null,
    ) {
        parent::__construct($generator, $analyzer, $locale);

        $this->setAdapter($adapter);
    }

    public function getAdapter(): adapter
    {
        return $this->adapter;
    }

    public function setAdapter(?adapter $adapter = null): static
    {
        $this->adapter = $adapter ?? new adapter();

        return $this;
    }

    /**
     * @param string $message
     */
    protected function valueIsSet($message = 'XML is undefined'): static
    {
        return parent::valueIsSet($message);
    }
}
