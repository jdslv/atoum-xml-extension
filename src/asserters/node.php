<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

use SimpleXmlElement;

/**
 * Assertion dedicated to a single node.
 */
class node extends variable
{
    protected bool $parseXml = true;

    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'attributes':
            case 'content':
            case 'nodes':
                return $this->{$asserter}();

            default:
                return parent::__get($asserter);
        }
    }

    /**
     * Return an `attribute` asserter.
     */
    public function attribute(string $name): attribute
    {
        $tmp = explode(':', $name);
        $prefix = null;

        if (count($tmp) === 2) {
            $prefix = $tmp[0];
            $name = $tmp[1];
        }

        foreach ($this->valueIsSet()->value->attributes() as $key => $value) {
            if ($key === $name) {
                $attribute = [
                    'name' => $key,
                    'value' => (string) $value,
                ];

                return $this->generator->__call(attribute::class, [$attribute])->setParent($this);
            }
        }

        foreach ($this->value->getDocNamespaces(true) as $p => $url) {
            if (!$prefix || $p === $prefix) {
                foreach ($this->value->attributes($p, true) as $key => $value) {
                    if ($key === $name) {
                        $attribute = [
                            'name' => $key,
                            'prefix' => $p,
                            'url' => $url,
                            'value' => (string) $value,
                        ];

                        return $this->generator->__call(attribute::class, [$attribute])->setParent($this);
                    }
                }
            }
        }

        return $this->fail($this->_('No attribute "%s"', $name));
    }

    /**
     * Return an `attributes` asserter based on node's attributes.
     */
    public function attributes(?string $name = null): attributes
    {
        $attributes = [];

        foreach ($this->valueIsSet()->value->attributes() as $key => $value) {
            if (!$name || $key === $name) {
                $attributes[] = [
                    'name' => $key,
                    'value' => (string) $value,
                ];
            }
        }

        foreach ($this->value->getDocNamespaces(true) as $prefix => $url) {
            foreach ($this->value->attributes($prefix, true) as $key => $value) {
                if (!$name || $key === $name) {
                    $attributes[] = [
                        'name' => $key,
                        'prefix' => $prefix,
                        'url' => $url,
                        'value' => (string) $value,
                    ];
                }
            }
        }

        return $this->generator->__call(attributes::class, [$attributes])->setParent($this);
    }

    /**
     * Return a `content` asserter based on node's value.
     */
    public function content(): content
    {
        return $this->valueIsSet()->generator->__call(content::class, [(string) $this->value])->setParent($this);
    }

    /**
     * Checks the node's tag name.
     */
    public function isTag(string $name, ?string $message = null): static
    {
        $current = $this->valueIsSet()->value->getName();

        if ($current === $name) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'Node is "<%s>" not "<%s>"', $current, $name));
        }

        return $this;
    }

    /**
     * Return a `node` asserter.
     */
    public function node(string $name): node
    {
        foreach ($this->valueIsSet()->value->children() as $key => $value) {
            if ($key === $name) {
                return $this->generator->__call(node::class, [$value])->setParent($this);
            }
        }

        return $this->fail($this->_('Node "<%s>" does not exist', $name));
    }

    /**
     * Return a `nodes` asserter based on node's childrens.
     */
    public function nodes(?string $name = null): nodes
    {
        $nodes = [];

        foreach ($this->valueIsSet()->value->children() as $key => $value) {
            if (!$name || $key === $name) {
                $nodes[] = $value;
            }
        }

        return $this->generator->__call(nodes::class, [$nodes])->setParent($this);
    }

    /**
     * @param array $value
     */
    public function setWith($value): static
    {
        parent::setWith($value);

        if ($this->parseXml) {
            if ($value instanceof SimpleXmlElement) {
                $this->pass();
            } elseif ($value === null) {
                $this->fail('XML is undefined');
            } else {
                $this->fail($this->_('"%s" is not a valid XML', $value));
            }
        }

        return $this;
    }

    /**
     * Return a `nodes` asserter from an XML path.
     */
    public function xpath(string $path, ?string $message = null): nodes
    {
        $nodes = @$this->valueIsSet()->value->xpath($path);

        if ($nodes === false || $nodes === null) {
            $this->fail($this->_($message ?? 'Path "%s" does not exist', $path));
        }

        $this->pass();

        return $this->valueIsSet()->generator->__call(nodes::class, [$nodes])->setParent($this);
    }
}
