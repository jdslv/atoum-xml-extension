<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

use atoum\atoum\xml\asserters\integer as integerAsserter;
use SimpleXmlElement;

/**
 * Assertion dedicated to nodes.
 */
class nodes extends variable
{
    protected int $position = -1;

    /**
     * @param string $method
     * @param mixed $arguments
     */
    public function __call($method, $arguments)
    {
        switch (strtolower($method)) {
            case 'areempty':
                return $this->isEmpty(...$arguments);

            case 'arenotempty':
                return $this->isNotEmpty(...$arguments);
        }

        return parent::__call($method, $arguments);
    }

    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'areempty':
            case 'arenotempty':
            case 'first':
            case 'isempty':
            case 'isnotempty':
            case 'last':
            case 'next':
            case 'nodes':
            case 'size':
                return $this->{$asserter}();

            default:
                return parent::__get($asserter);
        }
    }

    /**
     * Return the first node in the collection.
     */
    public function first(?string $message = null): node
    {
        return $this->item(0, $message ?? 'No first element');
    }

    /**
     * Checks the size of the collection.
     */
    public function hasSize(int $size, ?string $message = null): static
    {
        $count = count($this->valueIsSet()->value);

        if ($size < 0) {
            $this->fail($this->_($message ?? 'Value must be positive'));
        } elseif ($count === $size) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'Nodes has size %d, expected size %d', $count, $size));
        }

        return $this;
    }

    /**
     * Checks if the collection is empty.
     */
    public function isEmpty(?string $message = null): static
    {
        return $this->hasSize(0, $message ?? 'Nodes are not empty, has size %d');
    }

    /**
     * Checks if the collection is not empty.
     */
    public function isNotEmpty(?string $message = null): static
    {
        $count = count($this->valueIsSet()->value);

        if ($count > 0) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'Nodes are empty'));
        }

        return $this;
    }

    /**
     * Return a node in the collection depending on position.
     */
    public function item(int $position, ?string $message = null): node
    {
        if (!$this->adapter->array_key_exists($position, $this->valueIsSet()->value)) {
            $this->fail($message ?? $this->_('No element at position %d', $position));
        }

        $this->position = $position;

        return $this->pass()->generator->__call(node::class, [$this->value[$position]])->setParent($this);
    }

    /**
     * Return the last node in the collection.
     */
    public function last(?string $message = null): node
    {
        $count = $this->valueIsSet()->adapter->count($this->value);

        $this->position = $count - 1;

        return $this->item($this->position, $message ?? 'No last element');
    }

    /**
     * Return the next node in the collection.
     */
    public function next(?string $message = null): node
    {
        return $this->item(++$this->position, $message ?? 'No next element');
    }

    /**
     * Return a new nodes collection.
     */
    public function nodes(?string $name = null): static
    {
        $nodes = [];

        foreach ($this->valueIsSet()->value as $item) {
            foreach ($item->children() as $key => $value) {
                if (!$name || $key === $name) {
                    $nodes[] = $value;
                }
            }
        }

        return $this->generator->__call(static::class, [$nodes])->setParent($this);
    }

    /**
     * @param array $value
     */
    public function setWith($value): static
    {
        parent::setWith($value);

        if (!$this->adapter->is_array($value)) {
            $this->fail($this->_('Array expected'));
        } else {
            foreach ($value as $position => $item) {
                if ($item === null) {
                    $this->fail($this->_('XML is undefined at position %d', $position));
                } elseif (!($item instanceof SimpleXmlElement)) {
                    $this->fail($this->_('"%s" is not a valid XML at position %d', $item, $position));
                }
            }

            $this->pass();
        }

        return $this;
    }

    /**
     * Return an `integer` asserter based collection size.
     */
    public function size(): integerAsserter
    {
        return $this->valueIsSet()->generator->__call(integerAsserter::class, [count($this->value)])->setParent($this);
    }

    /**
     * Return a `nodes` asserter from an XML path.
     */
    public function xpath(string $path, ?string $message = null): nodes
    {
        $nodes = [];

        foreach ($this->valueIsSet()->value as $value) {
            $values = @$value->xpath($path);

            if ($values) {
                $nodes = array_merge($nodes, $values);
            }
        }

        if (!$nodes) {
            $this->fail($this->_($message ?? 'Path "%s" does not exist', $path));
        }

        $this->pass();

        return $this->valueIsSet()->generator->__call(nodes::class, [$nodes])->setParent($this);
    }
}
