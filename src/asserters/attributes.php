<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

use arrayAccess;
use atoum\atoum\exceptions;
use atoum\atoum\xml\asserters\integer as integerAsserter;

/**
 * Assertion dedicated to attributes.
 */
class attributes extends variable implements arrayAccess
{
    /**
     * @param string $method
     * @param mixed $arguments
     */
    public function __call($method, $arguments)
    {
        switch (strtolower($method)) {
            case 'areempty':
                return $this->isEmpty(...$arguments);

            case 'arenotempty':
                return $this->isNotEmpty(...$arguments);
        }

        return parent::__call($method, $arguments);
    }

    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'areempty':
            case 'arenotempty':
            case 'isempty':
            case 'isnotempty':
            case 'size':
                return $this->{$asserter}();

            case 'string':
                return $this;

            default:
                return parent::__get($asserter);
        }
    }

    /**
     * Checks that an attribute exists with a given name.
     */
    public function hasKey(string $key, ?string $message = null): static
    {
        if ($this->valueIsSet()->offsetExists($key)) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'No attribute key "%s" found', $key));
        }

        return $this;
    }

    /**
     * Checks that attributes exists with given names.
     */
    public function hasKeys(array $keys, ?string $message = null): static
    {
        $this->valueIsSet();

        foreach ($keys as $key) {
            $this->hasKey($key, $message);
        }

        return $this;
    }

    /**
     * Checks that attributes has a given size.
     */
    public function hasSize(int $size, ?string $message = null): static
    {
        $count = count($this->valueIsSet()->value);

        if ($size < 0) {
            $this->fail(
                $this->_($message ?? 'Value must be positive'),
            );
        } elseif ($count === $size) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'Attributes has size %d, expected size %d', $count, $size));
        }

        return $this;
    }

    /**
     * Checks that the attribute list is empty.
     */
    public function isEmpty(?string $message = null): static
    {
        return $this->hasSize(0, $message ?? 'Attributes are not empty, has size %d');
    }

    /**
     * Checks that the attribute list is not empty.
     */
    public function isNotEmpty(?string $message = null): static
    {
        $count = count($this->valueIsSet()->value);

        if ($count > 0) {
            $this->pass();
        } else {
            $this->fail($this->_($message ?? 'Attributes are empty'));
        }

        return $this;
    }

    /**
     * Checks that an attribute does not exist with a given name.
     */
    public function notHasKey(string $key, ?string $message = null): static
    {
        if ($this->valueIsSet()->offsetExists($key)) {
            $this->fail($this->_($message ?? 'Attribute key "%s" exists', $key));
        } else {
            $this->pass();
        }

        return $this;
    }

    /**
     * Checks that attributes does not exist with given names.
     */
    public function notHasKeys(array $keys, ?string $message = null): static
    {
        $this->valueIsSet();

        foreach ($keys as $key) {
            $this->notHasKey($key, $message);
        }

        return $this;
    }

    public function offsetExists($key): bool
    {
        $prefix = null;
        $name = $key;

        if ($this->valueIsSet()->adapter->strpos($name, ':') !== false) {
            [$prefix, $name] = $this->adapter->explode(':', $key);
        }

        foreach ($this->value as $item) {
            if ($item['name'] === $name) {
                $ns = $item['prefix'] ?? null;

                if (!$prefix || $prefix === $ns) {
                    return true;
                }
            }
        }

        return false;
    }

    public function offsetGet($key): attribute
    {
        $prefix = null;
        $name = $key;

        if ($this->valueIsSet()->adapter->strpos($name, ':') !== false) {
            [$prefix, $name] = $this->adapter->explode(':', $key);
        }

        if (!$this->valueIsSet()->offsetExists($key)) {
            $this->fail($this->_('No attribute "%s" found', $key));
        }

        $attributes = [];
        $backup = [];

        foreach ($this->value as $item) {
            if ($item['name'] === $name) {
                $ns = $item['prefix'] ?? null;

                if (!$prefix && $ns) {
                    $backup[] = $item;
                }

                if ($prefix === $ns) {
                    $attributes[] = $item;
                }
            }
        }

        $nbAttr = count($attributes);
        $nbBack = count($backup);
        $attribute = null;

        if ($nbAttr === 0 && $nbBack === 1) {
            $attribute = $backup[0];
        }

        if ($nbAttr === 1) {
            $attribute = $attributes[0];
        }

        if ($nbAttr > 1 || $nbBack > 1) {
            $this->fail($this->_('Too much attributes "%s" found', $key));
        }

        $this->pass();

        return $this->generator->__call(attribute::class, [$attribute])->setParent($this);
    }

    public function offsetSet($key, $value): void
    {
        throw new exceptions\logic('Array is read only');
    }

    public function offsetUnset($key): void
    {
        throw new exceptions\logic('Array is read only');
    }

    /**
     * @param array $value
     */
    public function setWith($value): static
    {
        if ($value === null) {
            $this->fail($this->_('Value is undefined'));
        } elseif (!$this->adapter->is_array($value)) {
            $this->fail($this->_('Value must be an array'));
        } else {
            foreach ($value as $position => $attribute) {
                if ($attribute === null) {
                    $this->fail($this->_('Value is undefined at position %d', $position));
                } elseif (!$this->adapter->is_array($attribute)) {
                    $this->fail($this->_('Value must be an array at position %d', $position));
                } elseif (!$this->adapter->array_key_exists('name', $attribute)) {
                    $this->fail($this->_('Name is missing in the attribute at position %d', $position));
                } elseif (!$this->adapter->array_key_exists('value', $attribute)) {
                    $this->fail($this->_('Value is missing in the attribute at position %d', $position));
                }
            }
        }

        $this->pass();

        return parent::setWith($value);
    }

    /**
     * Return an `integer` asserter based attributes size.
     */
    public function size(): integerAsserter
    {
        return $this->valueIsSet()->generator->__call(integerAsserter::class, [count($this->value)])->setParent($this);
    }

    /**
     * @param string $message
     */
    protected function valueIsSet($message = 'Value is undefined'): static
    {
        return parent::valueIsSet($message);
    }
}
