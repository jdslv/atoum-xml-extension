<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

use DomDocument;
use SimpleXmlElement;

class xml extends node
{
    protected bool $parseXml = false;

    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'namespaces':
            case 'root':
                return $this->{$asserter}();

            default:
                return parent::__get($asserter);
        }
    }

    /**
     * Return a `namespace` asserter from a given name.
     */
    public function namespace(string $prefix, string $uri): xmlNamespace
    {
        $exists = $this->valueIsSet()->value->getDocNamespaces(true);
        $data = [];

        if ($this->adapter->array_key_exists($prefix, $exists) && $exists[$prefix] === $uri) {
            $used = $this->valueIsSet()->value->getNamespaces(true);

            $data['prefix'] = $prefix;
            $data['uri'] = $uri;

            $data['isUsed'] = $this->adapter->array_key_exists($prefix, $used);
        }

        return $this->generator->__call(xmlNamespace::class, [$data])->setParent($this);
    }

    /**
     * Return a `namespaces` asserter.
     */
    public function namespaces(): namespaces
    {
        return $this->valueIsSet()->generator->__call(namespaces::class, [$this->value])->setParent($this);
    }

    /**
     * Moves the cursor to the root element.
     *
     * It's not an assertion, just a sugar to help in your test.
     */
    public function root(): static
    {
        return $this->valueIsSet();
    }

    /**
     * @param array $value
     */
    public function setWith($value): static
    {
        parent::setWith($value);

        if ($value === null) {
            $this->fail($this->getLocale()->_('XML is undefined'));
        } else {
            $data = null;
            $error = $this->adapter->libxml_use_internal_errors(true);

            if ($value instanceof SimpleXmlElement) {
                $data = $value;
            } elseif ($value instanceof DomDocument) {
                $data = $this->adapter->simplexml_import_dom($value);
            } elseif ($this->adapter->is_string($value) && $this->adapter->strpos($value, '<') === 0) {
                $data = $this->adapter->simplexml_load_string($value);
            }

            $this->adapter->libxml_use_internal_errors($error);

            if ($data instanceof SimpleXmlElement) {
                $this->pass()->value = $data;
            } else {
                $this->fail($this->getLocale()->_('"%s" is not a valid XML', $value));
            }
        }

        return $this;
    }
}
