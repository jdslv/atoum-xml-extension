<?php

declare(strict_types=1);

namespace atoum\atoum\xml\asserters;

use atoum\atoum\asserters;
use atoum\atoum\xml\traits;

/**
 * Asserter dedicated to integers.
 *
 * Overrided to allow our asserter traversing.
 */
class integer extends asserters\integer
{
    use traits\parentTrait;
}
