<?php

declare(strict_types=1);

namespace atoum\atoum\xml\traits;

use atoum\atoum\exceptions;
use atoum\atoum\xml\asserters\variable;

trait parentTrait
{
    protected ?variable $parent = null;

    /**
     * @param string $method
     * @param mixed $arguments
     */
    public function __call($method, $arguments)
    {
        if ($this->parent !== null) {
            return $this->parent->{$method}(...$arguments);
        }

        return parent::__call($method, $arguments);
    }

    /**
     * @param string $asserter
     */
    public function __get($asserter)
    {
        switch (strtolower($asserter)) {
            case 'parent':
                return $this->{$asserter}();

            default:
                if ($this->parent !== null) {
                    return $this->parent->__get($asserter);
                }

                return parent::__get($asserter);
        }
    }

    public function parent(): ?variable
    {
        return $this->parentIsSet()->parent;
    }

    protected function parentIsSet(string $message = 'Parent node is undefined'): static
    {
        if ($this->parent === null) {
            throw new exceptions\logic($message);
        }

        return $this;
    }

    public function setParent(variable $node): static
    {
        if ($node === $this) {
            throw new exceptions\logic('Can not add self as parent');
        }

        $this->parent = $node;

        return $this;
    }
}
