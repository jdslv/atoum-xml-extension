<?php

declare(strict_types=1);

namespace atoum\atoum\xml;

use atoum\atoum;
use atoum\atoum\configurator;
use atoum\atoum\observable;
use atoum\atoum\runner;
use atoum\atoum\test;

class extension implements atoum\extension
{
    public function __construct(?configurator $configurator = null)
    {
        if ($configurator !== null) {
            $parser = $configurator->getScript()->getArgumentsParser();

            $handler = function ($script, $argument, array $values) {
                $base = dirname(__DIR__);
                $script->getRunner()
                    ->addTestsFromDirectory($base . '/tests/functionals')
                    ->addTestsFromDirectory($base . '/tests/units')
                ;
            };

            $parser->addHandler($handler, ['--test-ext']);
        }
    }

    /**
     * @param string $event
     */
    public function handleEvent($event, observable $observable): void {}

    public function setRunner(runner $runner): static
    {
        return $this;
    }

    public function setTest(test $test): static
    {
        $handler = function ($data = null, $depth = null, $options = null) use ($test) {
            $asserter = new asserters\xml($test->getAsserterGenerator());

            return $asserter->setWithTest($test)->setWith($data, $depth, $options);
        };

        $test->getAssertionManager()->setHandler('xml', $handler);

        return $this;
    }
}
