# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.1] - 2025-02-03

### Added
- `namespaces::areEmpty()` / `namespaces::areNotEmpty()` asserters
- `namespaces::hasSize()` asserter
- `namespaces::isEmpty()` / `namespaces::isNotEmpty()` asserters
- `namespaces::size()` asserter

### Changed
- Better error messages
- Better types


## [1.1.0] - 2023-02-01

### Added
- Dev Container configuration
- `php-cs-fixer` configuration

### Removed
- PHP7 & 8.0 support


## [1.0.0] - 2021-08-03

First version
