
# jdslv/atoum-xml-extension

> The atoum XML extension allows you to make assertions on XML files

[![GitLab](https://img.shields.io/static/v1?message=GitLab&logo=gitlab&color=grey&label=)](https://gitlab.com/jdslv/atoum-xml-extension)
[![Latest stable version](https://img.shields.io/packagist/v/jdslv/atoum-xml-extension)](https://packagist.org/packages/jdslv/atoum-xml-extension)
[![Build status](https://gitlab.com/jdslv/atoum-xml-extension/badges/main/pipeline.svg)](https://gitlab.com/jdslv/atoum-xml-extension/-/pipelines)
[![Coverage status](https://img.shields.io/codecov/c/gitlab/jdslv/atoum-xml-extension)](https://codecov.io/gl/jdslv/atoum-xml-extension/)
[![Minimal PHP version](https://img.shields.io/packagist/php-v/jdslv/atoum-xml-extension)](https://gitlab.com/jdslv/atoum-xml-extension)
[![License](https://img.shields.io/packagist/l/jdslv/atoum-xml-extension)](https://gitlab.com/jdslv/atoum-xml-extension/-/blob/main/LICENSE)


This atoum extension allows you to test XML document using [atoum](https://github.com/atoum/atoum).

This repository is based on [shulard/atoum-xml-extension](https://github.com/shulard/atoum-xml-extension).


## Install it

Install extension using [composer](https://getcomposer.org):

```
composer require --dev jdslv/atoum-xml-extension
```

The extension is automatically added to atoum configuration.


## Use it

Add the following code to your configuration file:

```php
<?php

// .atoum.php

$runner->addExtension(new atoum\atoum\xml\extension($script));
```

```php
<?php

// tests example

namespace my\project\tests\unit;

use atoum;

class MyClass extends atoum\atoum\test
{
    public function testXML()
    {
        $this
            ->if($xml = <<<XML
<?xml version="1.0" ?>
<root>
    <node attribute="value" />
    <node m:attribute="namespaced value" />
</root>
XML)
            ->then
                ->xml($xml)
                    ->attributes
                        ->areEmpty
                    ->namespaces
                        ->areEmpty

                    ->nodes
                        ->hasSize(2)
                        ->first
                            ->isTag('node')
                            ->attributes
                                ->hasSize(1)
                                ->hasKey('attribute')
                                ->string['attribute']
                                    ->isIdenticalTo('value')
                                    ->hasNotNamespace
                            ->content
                                ->isEmpty

                        ->next
                            ->isTag('node')
                            ->attributes
                                ->hasSize(1)
                                ->hasKey('m:attribute')
                                ->string['attribute']
                                    ->isIdenticalTo('namespaced value')
                                    ->hasNamespace('m')
                                ->string['m:attribute']
                                    ->isIdenticalTo('namespaced value')
                            ->content
                                ->isEmpty
        ;
    }
}
```


## Asserters

Every asserters allow a final `string` argument to personalize error message.

They are all fluent, you can chain assertions, we automatically find the better context for your asserters.

You should also know that all assertions without parameter can be written with or without parenthesis.
So `$this->integer(0)->isZero()` is the same as `$this->integer(0)->isZero`.

### attribute

It's the assertion dedicated to a single attribute.

#### contains

`contains` checks that the attribute value contain a given string.

```
public function contains(string $value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->contains('bar')  // succeed
            ->contains('baz')  // failed
;
```

#### endWith

`endWith` checks that the attribute value ends with a given string.

```
public function endWith(string $value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->endWith('r')  // succeed
            ->endWith('z')  // failed
;
```

#### hasNamespace

`hasNamespace` checks that the attribute name have a namespace.

```
public function hasNamespace(?string $prefix = null, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:ex="http://example.com" ex:foo="bar" abc="def">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->hasNamespace()       // succeed
            ->hasNamespace('ex')   // succeed
            ->hasNamespace('bad')  // failed
        ->attribute('abc')
            ->hasNamespace()       // failed
;
```

#### hasNotNamespace

`hasNotNamespace` checks that the attribute name have not a namespace.

```
public function hasNotNamespace(?string $prefix = null, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:ex="http://example.com" ex:foo="bar" abc="def">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->hasNotNamespace()       // failed
            ->hasNotNamespace('ex')   // failed
            ->hasNotNamespace('bad')  // succeed
        ->attribute('abc')
            ->hasNotNamespace()       // succeed
;
```

#### isEmpty

`isEmpty` checks that the attribute value is empty.

```
public function isEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar" baz="">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->isEmpty()     // failed
        ->attribute('baz')
            ->isEmpty()     // succeed
;
```

#### isEqualTo

`isEqualTo` checks that the attribute value is equal to a given string.

```
public function isEqualTo($value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->isEqualTo('bar')  // succeed
            ->isEqualTo('baz')  // failed
;
```

#### isIdenticalTo

`isIdenticalTo` checks that the attribute value is identical to a given string.

```
public function isIdenticalTo($value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->isIdenticalTo('bar')  // succeed
            ->isIdenticalTo('baz')  // failed
;
```

#### isNotEmpty

`isNotEmpty` checks that the attribute value is not empty.

```
public function isNotEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar" baz="">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->isNotEmpty()  // succeed
        ->attribute('baz')
            ->isNotEmpty()  // failed
;
```

#### isNotEqualTo

`isNotEqualTo` checks that the attribute value is not equal to a given string.

```
public function isNotEqualTo($value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->isNotEqualTo('bar')  // failed
            ->isNotEqualTo('baz')  // succeed
;
```

#### isNotIdenticalTo

`isNotIdenticalTo` checks that the attribute value is not identical to a given string.

```
public function isNotIdenticalTo($value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->isNotIdenticalTo('bar')  // failed
            ->isNotIdenticalTo('baz')  // succeed
;
```

#### matches

`matches` checks that the attribute value match a pattern.

```
public function matches(string $pattern, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->matches('/\w{3}/')  // succeed
            ->matches('/^[^b]/')  // failed
;
```

#### notContains

`notContains` checks that the attribute value does not contain a given string.

```
public function notContains(string $value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->notContains('bar')  // failed
            ->notContains('baz')  // succeed
;
```

#### notEndWith

`notEndWith` checks that the attribute value does not end with a given string.

```
public function notEndWith(string $value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->notEndWith('r')  // failed
            ->notEndWith('z')  // succeed
;
```

#### notMatches

`notMatches` checks that the attribute value does not match a pattern.

```
public function notMatches(string $pattern, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->notMatches('/\w{3}/')  // failed
            ->notMatches('/^[^b]/')  // succeed
;
```

#### notStartWith

`notStartWith` checks that the attribute value does not start with a given string.

```
public function notStartWith(string $value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->notStartWith('a')  // succeed
            ->notStartWith('b')  // failed
;
```

#### startWith

`startWith` checks that the attribute value starts with a given string.

```
public function startWith(string $value, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attribute('foo')
            ->startWith('a')  // failed
            ->startWith('b')  // succeed
;
```


### attributes

It's the assertion dedicated to attributes.

#### areEmpty

`areEmpty` checks that the attribute list is empty.

```
public function areEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->areEmpty()      // failed
        ->node('node')
            ->attributes()
                ->areEmpty()  // succeed
;
```

#### areNotEmpty

`areNotEmpty` checks that the attribute list is not empty.

```
public function areNotEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->areNotEmpty()      // succeed
        ->node('node')
            ->attributes()
                ->areNotEmpty()  // failed
;
```

#### hasKey

`hasKey` checks that an attribute exists with a given name.

```
public function hasKey(string $key, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->hasKey('foo')  // succeed
            ->hasKey('bar')  // failed
;
```

#### hasKeys

`hasKeys` checks that attributes exists with given names.

```
public function hasKeys(array $keys, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar" bar="baz">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->hasKeys(['foo', 'bar'])  // succeed
            ->hasKeys(['foo', 'baz'])  // failed
            ->hasKeys(['baz'])         // failed
;
```

#### hasSize

`hasSize` checks that attributes has a given size.

```
public function hasSize(int $size, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar" bar="baz">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->hasSize(2)  // succeed
            ->hasSize(3)  // failed
;
```

#### isEmpty

`isEmpty` checks that the attribute list is empty.

```
public function isEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->isEmpty()      // failed
        ->node('node')
            ->attributes()
                ->isEmpty()  // succeed
;
```

#### isNotEmpty

`isNotEmpty` checks that the attribute list is not empty.

```
public function isNotEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->isNotEmpty()      // succeed
        ->node('node')
            ->attributes()
                ->isNotEmpty()  // failed
;
```

#### notHasKey

`notHasKey` checks that an attribute does not exist with a given name.

```
public function notHasKey(string $key, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->notHasKey('foo')  // failed
            ->notHasKey('bar')  // succeed
;
```

#### notHasKeys

`notHasKeys` checks that attributes does not exist with given names.

```
public function notHasKeys(array $keys, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar" bar="baz">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->notHasKeys(['foo', 'bar'])  // failed
            ->notHasKeys(['foo', 'baz'])  // failed
            ->notHasKeys(['baz'])         // succeed
;
```

#### size

`size` return an `integer` asserter based attributes size.

```
public function size(): atoum\atoum\xml\asserters\integer
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root foo="bar" bar="baz">
    <node />
</root>';

$this
    ->xml($xml)
        ->attributes()
            ->size()
                ->isEqualTo(2)  // succeed
;
```


### content

It's the asserter dedicated to node's content.

#### contains

```
public function contains($fragment, ?string $message = null)
```

> `contains` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::contains](https://atoum.readthedocs.io/en/latest/asserters.html#string-contains).


#### endWith

```
public function endWith($fragment, ?string $message = null)
```

> `endWith` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::endWith](https://atoum.readthedocs.io/en/latest/asserters.html#string-end-with).


#### isEmpty

`isEmpty` checks if the content is empty.

```
public function isEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        baz
    </foo>
    <bar />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->next()
                ->content()
                    ->isEmpty()  // failed
            ->next()
                ->content()
                    ->isEmpty()  // succeed
;
```

#### isEqualTo

```
public function isEqualTo($value, ?string $message = null)
```

> `isEqualTo` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::isEqualTo](https://atoum.readthedocs.io/en/latest/asserters.html#string-is-equal-to).


#### isIdenticalTo

```
public function isIdenticalTo($value, ?string $message = null)
```

> `isIdenticalTo` is a method inherited from the `variable` asserter.
For more information, refer to the documentation of [variable::isIdenticalTo](https://atoum.readthedocs.io/en/latest/asserters.html#variable-is-identical-to).


#### isNotEmpty

`isNotEmpty` checks if the content is not empty.

```
public function isNotEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        baz
    </foo>
    <bar />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->next()
                ->content()
                    ->isNotEmpty()  // succeed
            ->next()
                ->content()
                    ->isNotEmpty()  // failed
;
```

#### isNotEqualTo

```
public function isNotEqualTo($value, ?string $message = null)
```

> `isNotEqualTo` is a method inherited from the `variable` asserter.
For more information, refer to the documentation of [variable::isNotEqualTo](https://atoum.readthedocs.io/en/latest/asserters.html#variable-is-not-equal-to).


#### isNotIdenticalTo

```
public function isNotIdenticalTo($value, ?string $message = null)
```

> `isNotIdenticalTo` is a method inherited from the `variable` asserter.
For more information, refer to the documentation of [variable::isNotIdenticalTo](https://atoum.readthedocs.io/en/latest/asserters.html#variable-is-not-identical-to).


#### match

```
public function match($pattern, ?string $message = null)
```

> `match` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::match](https://atoum.readthedocs.io/en/latest/asserters.html#string-match).


#### matches

```
public function matches($pattern, ?string $message = null)
```

> `matches` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::matches](https://atoum.readthedocs.io/en/latest/asserters.html#string-matches).


#### notContains

```
public function notContains($fragment, ?string $message = null)
```

> `notContains` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::notContains](https://atoum.readthedocs.io/en/latest/asserters.html#string-not-contains).


#### notEndWith

```
public function notEndWith($fragment, ?string $message = null)
```

> `notEndWith` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::notEndWith](https://atoum.readthedocs.io/en/latest/asserters.html#string-not-end-with).


#### notMatches

```
public function notMatches($pattern, ?string $message = null)
```

> `notMatches` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::notMatches](https://atoum.readthedocs.io/en/latest/asserters.html#string-not-matches).


#### notStartWith

```
public function notStartWith($fragment, ?string $message = null)
```

> `notStartWith` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::notStartWith](https://atoum.readthedocs.io/en/latest/asserters.html#string-not-start-with).


#### startWith

```
public function startWith($fragment, ?string $message = null)
```

> `startWith` is a method inherited from the `string` asserter.
For more information, refer to the documentation of [string::startWith](https://atoum.readthedocs.io/en/latest/asserters.html#string-start-with).



### namespace

It's the assertion dedicated to a single namespace.

#### exists

`exists` check if the namespace exists in the document.

```
public function exists(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespace('foo', 'http://example.com')
            ->exists()                            // succeed
        ->namespace('foo', 'http://example.org')
            ->exists()                            // failed
        ->namespace('bar', 'http://example.com')
            ->exists()                            // failed
;
```

#### isNotUsed

`isNotUsed` check if the namespace is not used in the document.

```
public function isNotUsed(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com" xmlns:bar="http://example.org">
    <node foo:attribute="value" />
</root>';

$this
    ->xml($xml)
        ->namespace('foo', 'http://example.com')
            ->isNotUsed()                         // failed
        ->namespace('bar', 'http://example.org')
            ->isNotUsed()                         // succeed
;
```

#### isUsed

`isUsed` check if the namespace is used in the document.

```
public function isUsed(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com" xmlns:bar="http://example.org">
    <node foo:attribute="value" />
</root>';

$this
    ->xml($xml)
        ->namespace('foo', 'http://example.com')
            ->isUsed()                            // succeed
        ->namespace('bar', 'http://example.org')
            ->isUsed()                            // failed
;
```

#### notExists

`notExists` check if the namespace does not exist in the document.

```
public function notExists(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespace('foo', 'http://example.com')
            ->notExists()                         // failed
        ->namespace('foo', 'http://example.org')
            ->notExists()                         // succeed
        ->namespace('bar', 'http://example.com')
            ->notExists()                         // succeed
;
```


### namespaces

It's the assertion dedicated to namespaces.

#### areEmpty

`areEmpty` checks that the namespace list is empty.

```
public function areEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->areEmpty()  // succeed
;
```

#### areNotEmpty

`areNotEmpty` checks that the namespace list is not empty.

```
public function areNotEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->areNotEmpty()  // failed
;
```

#### contains

`contains` checks that namespaces contain a given string.

```
public function contains(string $prefix, string $uri, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->contains('foo', 'http://example.com')  // succeed
        ->namespaces()
            ->contains('foo', 'http://example.org')  // failed
        ->namespaces()
            ->contains('bar', 'http://example.com')  // failed
;
```

#### hasNotPrefix

`hasNotPrefix` checks that namespaces have not a given prefix.

```
public function hasNotPrefix(string $prefix, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->hasNotPrefix('foo')  // failed
        ->namespaces()
            ->hasNotPrefix('bar')  // succeed
;
```

#### hasNotUri

`hasNotUri` checks that namespaces have not a given URI.

```
public function hasNotUri(string $uri, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->hasNotUri('http://example.com')  // failed
        ->namespaces()
            ->hasNotUri('http://example.org')  // succeed
        ->namespaces()
            ->hasNotUri('http://example.com')  // failed
;
```

#### hasPrefix

`hasPrefix` checks that namespaces have a given prefix.

```
public function hasPrefix(string $prefix, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->hasPrefix('foo')  // succeed
        ->namespaces()
            ->hasPrefix('bar')  // failed
;
```

#### hasSize

`hasSize` checks that namespaces has a given size.

```
public function hasSize(int $size, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->hasSize(1)  // succeed
            ->hasSize(2)  // failed
;
```

#### hasUri

`hasUri` checks that namespaces have a given URI.

```
public function hasUri(string $uri, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->hasUri('http://example.com')  // succeed
        ->namespaces()
            ->hasUri('http://example.org')  // failed
        ->namespaces()
            ->hasUri('http://example.com')  // succeed
;
```

#### isEmpty

`isEmpty` checks that the namespace list is empty.

```
public function isEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->isEmpty()  // succeed
;
```

#### isNotEmpty

`isNotEmpty` checks that the namespace list is not empty.

```
public function isNotEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->isNotEmpty()  // failed
;
```

#### notContains

`notContains` checks that namespaces do not contain a given string.

```
public function notContains(string $prefix, string $uri, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->notContains('foo', 'http://example.com')  // failed
        ->namespaces()
            ->notContains('foo', 'http://example.org')  // failed
        ->namespaces()
            ->notContains('bar', 'http://example.com')  // failed
;
```

#### size

`size` return an `integer` asserter based attributes size.

```
public function size(): atoum\atoum\xml\asserters\integer
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root xmlns:foo="http://example.com">
    <node />
</root>';

$this
    ->xml($xml)
        ->namespaces()
            ->size()
                ->isEqualTo(1)  // succeed
                ->isEqualTo(2)  // failed
;
```


### node

It's the assertion dedicated to a single node.

#### attribute

`attribute` return an `attribute` asserter.

```
public function attribute(string $name): atoum\atoum\xml\asserters\attribute
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <node foo="bar" />
    <node />
</root>';

$this
    ->xml($xml)
        ->node('node')
            ->attribute('foo')
                ->isEqualTo('bar')  // succeed
            ->attribute('bar')      // failed
;
```

#### attributes

`attributes` return an `attributes` asserter based on node's attributes.

```
public function attributes(?string $name = null): atoum\atoum\xml\asserters\attributes
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <node foo="bar" />
    <node />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->item(0)
                ->attributes()
                    ->isEmpty()  // failed
            ->item(1)
                ->attributes()
                    ->isEmpty()  // succeed
;
```

#### content

`content` return a `content` asserter based on node's value.

```
public function content(): atoum\atoum\xml\asserters\content
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <node>
        foo
    </node>
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->item(0)
                ->content()
                    ->contains('foo')  // succeed
            ->item(0)
                ->content()
                    ->contains('bar')  // failed
;
```

#### isTag

`isTag` checks the node's tag name.

```
public function isTag(string $name, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo />
    <bar />
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->item(0)
                ->isTag('foo')  // succeed
            ->item(1)
                ->isTag('foo')  // failed
;
```

#### node

`node` return a `node` asserter.

```
public function node(string $name): atoum\atoum\xml\asserters\node
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        <bar />
    </foo>
    <baz />
</root>';

$this
    ->xml($xml)
        ->node('foo')
            ->content()
                ->isEmpty()  // failed
        ->node('bar')        // succeed
;
```

#### nodes

`nodes` return a `nodes` asserter based on node's childrens.

```
public function nodes(?string $name = null): atoum\atoum\xml\asserters\nodes
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        <bar />
    </foo>
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->item(0)
                ->nodes()
                    ->isEmpty()  // failed
            ->item(1)
                ->nodes()
                    ->isEmpty()  // succeed
;
```

#### xpath

`xpath` return a `nodes` asserter from an XML path.

```
public function xpath(string $path, ?string $message = null): atoum\atoum\xml\asserters\nodes
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        <bar />
    </foo>
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->xpath('//foo/bar')
                ->isNotEmpty()    // succeed
            ->xpath('//foo/baz')  // failed
;
```


### nodes

It's the assertion dedicated to nodes.

#### areEmpty

`areEmpty` checks if the collection is empty.

```
public function areEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        <bar />
    </foo>
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes('foo')
            ->areEmpty()  // failed
        ->nodes('baz')
            ->areEmpty()  // succeed
;
```

#### areNotEmpty

`areNotEmpty` checks if the collection is not empty.

```
public function areNotEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        <bar />
    </foo>
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes('foo')
            ->areNotEmpty()  // succeed
        ->nodes('baz')
            ->areNotEmpty()  // failed
;
```

#### first

`first` return the first node in the collection.

```
public function first(?string $message = null): atoum\atoum\xml\asserters\node
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo />
    <bar />
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->first()
                ->isTag('foo')  // succeed
;
```

#### hasSize

`hasSize` checks the size of the collection.

```
public function hasSize(int $size, ?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo />
    <bar />
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->hasSize(3)  // succeed
            ->hasSize(2)  // failed
;
```

#### isEmpty

`isEmpty` checks if the collection is empty.

```
public function isEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        <bar />
    </foo>
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes('foo')
            ->isEmpty()  // failed
        ->nodes('baz')
            ->isEmpty()  // succeed
;
```

#### isNotEmpty

`isNotEmpty` checks if the collection is not empty.

```
public function isNotEmpty(?string $message = null): static
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        <bar />
    </foo>
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes('foo')
            ->isNotEmpty()  // succeed
        ->nodes('baz')
            ->isNotEmpty()  // failed
;
```

#### item

`item` return a node in the collection depending on position.

```
public function item(int $position, ?string $message = null): atoum\atoum\xml\asserters\node
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo />
    <bar />
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->item(1)
                ->isTag('bar')  // succeed
            ->item(4)           // failed
;
```

#### last

`last` return the last node in the collection.

```
public function last(?string $message = null): atoum\atoum\xml\asserters\node
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo />
    <bar />
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->last()
                ->isTag('baz')  // succeed
;
```

#### next

`next` return the next node in the collection.

```
public function next(?string $message = null): atoum\atoum\xml\asserters\node
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo />
    <bar />
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->next()
                ->isTag('foo')  // succeed
            ->next()
                ->isTag('bar')  // succeed
            ->next()
                ->isTag('baz')  // succeed
            ->next()            // failed
;
```

#### size

`size` return an `integer` asserter based collection size.

```
public function size(): atoum\atoum\xml\asserters\integer
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo />
    <bar />
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->size()
                ->isGreaterThan(1)  // succeed
;
```

#### xpath

`xpath` return a `nodes` asserter from an XML path.

```
public function xpath(string $path, ?string $message = null): atoum\atoum\xml\asserters\nodes
```

```php
<?php

$xml = '<?xml version="1.0" ?>
<root>
    <foo>
        <bar />
    </foo>
    <baz />
</root>';

$this
    ->xml($xml)
        ->nodes()
            ->xpath('//foo/bar')
                ->isNotEmpty()    // succeed
            ->xpath('//foo/baz')  // failed
;
```


### xml

#### attribute

`attribute` return an `attribute` asserter.

```
public function attribute(string $name): atoum\atoum\xml\asserters\attribute
```

> `attribute` is a method inherited from the `node` asserter.
For more information, refer to the documentation of [node::attribute](#node-attribute).


#### attributes

`attributes` return an `attributes` asserter based on node's attributes.

```
public function attributes(?string $name = null): atoum\atoum\xml\asserters\attributes
```

> `attributes` is a method inherited from the `node` asserter.
For more information, refer to the documentation of [node::attributes](#node-attributes).


#### content

`content` return a `content` asserter based on node's value.

```
public function content(): atoum\atoum\xml\asserters\content
```

> `content` is a method inherited from the `node` asserter.
For more information, refer to the documentation of [node::content](#node-content).


#### isTag

`isTag` checks the node's tag name.

```
public function isTag(string $name, ?string $message = null): static
```

> `isTag` is a method inherited from the `node` asserter.
For more information, refer to the documentation of [node::isTag](#node-is-tag).


#### namespace

`namespace` return a `namespace` asserter from a given name.

```
public function namespace(string $prefix, string $uri): atoum\atoum\xml\asserters\xmlNamespace
```

#### namespaces

`namespaces` return a `namespaces` asserter.

```
public function namespaces(): atoum\atoum\xml\asserters\namespaces
```

#### node

`node` return a `node` asserter.

```
public function node(string $name): atoum\atoum\xml\asserters\node
```

> `node` is a method inherited from the `node` asserter.
For more information, refer to the documentation of [node::node](#node-node).


#### nodes

`nodes` return a `nodes` asserter based on node's childrens.

```
public function nodes(?string $name = null): atoum\atoum\xml\asserters\nodes
```

> `nodes` is a method inherited from the `node` asserter.
For more information, refer to the documentation of [node::nodes](#node-nodes).


#### parent

```
public function parent(): ?atoum\atoum\xml\asserters\variable
```

> `parent` is a method inherited from the `variable` asserter.
For more information, refer to the documentation of [variable::parent](#variable-parent).


#### root

`root` moves the cursor to the root element.

It's not an assertion, just a sugar to help in your test.

```
public function root(): static
```

#### xpath

`xpath` return a `nodes` asserter from an XML path.

```
public function xpath(string $path, ?string $message = null): atoum\atoum\xml\asserters\nodes
```

> `xpath` is a method inherited from the `node` asserter.
For more information, refer to the documentation of [node::xpath](#node-xpath).



## License

`jdslv/atoum-xml-extension` is released under the Apache 2.0 License.
See the bundled [LICENSE](LICENSE) file for details.

![atoum](http://atoum.org/images/logo/atoum.png)
