<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$finder = (new Finder())
    ->in(__DIR__)
;

return (new Config())
    ->setCacheFile(__DIR__ . '/vendor/.cache/php-cs-fixer')
    ->setRiskyAllowed(true)
    ->setRules([
        '@PhpCsFixer' => true,
        'assign_null_coalescing_to_coalesce_equal' => true,
        'class_keyword' => true,
        'concat_space' => [
            'spacing' => 'one',
        ],
        'get_class_to_class_keyword' => true,
        'fully_qualified_strict_types' => [
            'import_symbols' => false,
            'leading_backslash_in_global_namespace' => false,
        ],
        'global_namespace_import' => [
            'import_classes' => true,
            'import_constants' => true,
        ],
        'increment_style' => false,
        'list_syntax' => true,
        'method_chaining_indentation' => false,
        'new_with_parentheses' => false,
        'no_superfluous_elseif' => true,
        'no_unneeded_braces' => [
            'namespaces' => true,
        ],
        'no_unneeded_control_parentheses' => [
            'statements' => [
                'break',
                'clone',
                'continue',
                'echo_print',
                'others',
                'return',
                'switch_case',
                'yield',
                'yield_from',
            ],
        ],
        'no_useless_else' => true,
        'ordered_class_elements' => [
            'order' => [
                'use_trait',
            ],
            'sort_algorithm' => 'alpha',
        ],
        'php_unit_test_class_requires_covers' => false,
        'phpdoc_align' => false,
        'phpdoc_annotation_without_dot' => false,
        'phpdoc_no_alias_tag' => false,
        'phpdoc_scalar' => false,
        'phpdoc_to_comment' => [
            'ignored_tags' => [
                'var',
                'phpstan-var',
            ],
        ],
        'phpdoc_types_order' => [
            'null_adjustment' => 'always_last',
        ],
        'simplified_null_return' => true,
        'strict_param' => true,
        'switch_continue_to_break' => true,
        'trailing_comma_in_multiline' => [
            'after_heredoc' => true,
            'elements' => [
                'arguments',
                'array_destructuring',
                'arrays',
                'match',
                'parameters',
            ],
        ],
        'yoda_style' => false,
    ])
    ->setFinder($finder)
;
